# ==================================
# Base stage
# ==================================

FROM node:22-alpine as base

WORKDIR /var/www

CMD ["npm", "run", "dev"]

# ==================================
# Build tools
# ==================================

FROM base as build

# Reqiures:
#  - extra packages for image optimization
#  - docker for building containers
RUN apk add --no-cache \
    autoconf \
    automake \
    docker \
    g++ \
    libpng-dev \
    libtool \
    make \
    nasm

# ==================================
# Used in CI tests and builds
# ==================================

FROM build as ci

COPY .yarn /var/.yarn

# ==================================
# Local development stage
# ==================================

FROM build as local

# ==================================
# Production
# ==================================

FROM base as production

ENV NODE_ENV=production

COPY . /var/www

RUN apk add --no-cache dumb-init \
	# Remove stupid empty folder
	&& rm -rf /var/www/.yarn

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["npm", "run", "start"]
