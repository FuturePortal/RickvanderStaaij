# Rick.nu (Rick van der Staaij)

I've made my personal website open-source, because, why not?

## Setting up the local environment

The local setup requires you to have:

- Docker with Docker Compose
- [Git](https://git-scm.com/)
- [npm](https://www.npmjs.com/get-npm)
- yarn (`npm install --global yarn`)

Now to create the local environment; run `./Taskfile init`.

Check out all available development commands by running: `./Taskfile`.
