class AnimationDelay {
	delay = 1600;

	getDelay() {
		return this.delay;
	}

	clearDelay() {
		this.delay = 0;
	}
}

export default new AnimationDelay();
