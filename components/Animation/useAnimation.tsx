import { useEffect, useState } from 'react';

import AnimationDelay from 'components/Animation/AnimationDelay';

type IntroAnimation =
	| 'rollIn'
	| 'fadeIn'
	| 'fadeInLeft'
	| 'fadeInUp'
	| 'fadeInRight'
	| 'fadeInDown'
	| 'zoomIn'
	| 'backInDown';
type OutroAnimation =
	| 'fadeOut'
	| 'fadeOutRight'
	| 'fadeOutLeft'
	| 'fadeOutDown'
	| 'fadeOutUp'
	| 'zoomOut'
	| 'zoomOutUp'
	| 'backOutUp'
	| 'flipOutY'
	| 'flipOutX'
	| 'rollOut'
	| 'rotateOut'
	| 'rotateOutDownLeft'
	| 'rotateOutDownRight'
	| 'rotateOutUpLeft'
	| 'rotateOutUpRight';

type AnimationProps = {
	className: string;
	style: {
		animationDelay: string;
	};
};

const useAnimation = (
	intro: IntroAnimation = 'fadeIn',
	outro: OutroAnimation = 'fadeOut',
	delay = 0
): AnimationProps => {
	const [outroStarted, setOutroStarted] = useState(false);

	delay = delay + AnimationDelay.getDelay();

	useEffect(() => {
		const onPageOutro = () => setOutroStarted(true);

		window.addEventListener('page-outro', onPageOutro);

		return () => window.removeEventListener('page-outro', onPageOutro);
	});

	return {
		className: `animate__animated animate__${outroStarted ? outro : intro}`,
		style: {
			animationDelay: outroStarted || !delay ? '0s' : `${delay}ms`,
		},
	};
};

export default useAnimation;
