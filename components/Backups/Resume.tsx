// import React, { ReactElement } from 'react';

// import useAnimation from 'components/Animation';
// import Container from 'components/Layout/Container';
// import PageMeta from 'components/Layout/PageMeta';
// import Section from 'components/Layout/Section';

// const Underline = styled.span`
// 	text-decoration: underline;
// 	display: block;
// `;

// const Old = styled.span`
// 	color: ${({ theme }) => theme.color.gray40};
// `;

// const Page = (): ReactElement => {
// 	const animation = useAnimation('fadeInRight', 'fadeOutLeft');

// 	return (
// 		<Section>
// 			<Container {...animation}>
// 				<PageMeta title="My resume" description="The resume of Rick van der Staaij." />
// 				<h1>My resume</h1>
// 				<p>
// 					<strong>Personalia:</strong>
// 					<br />
// 					Rick van der Staaij.
// 					<br />
// 					Date of birth: 8 november 1991.
// 					<br />
// 					Birth place: Amersfoort.
// 					<br />
// 					Dutch nationality.
// 				</p>
// 				<p>
// 					<strong>Languages:</strong>
// 					<br />
// 					Dutch: full professional proficiency.
// 					<br />
// 					English: full professional proficiency.
// 				</p>
// 				<p>
// 					<strong>Work Experience:</strong>
// 					<br />
// 					<Underline>Sep 2022 - Ongoing...</Underline>
// 					Senior javascript developer (frontend) at Lab Digital in Utrecht.
// 					<br />
// 					Full-time (40 hours).
// 				</p>
// 				<p>
// 					<Underline>Apr 2022 - Aug 2022</Underline>
// 					Senior frontend developer at KVK in Utrecht.
// 					<br />
// 					Full-time (40 hours).
// 				</p>
// 				<p>
// 					<Underline>Feb 2014 - Mar 2022</Underline>
// 					Allround developer at Enrise in Amersfoort.
// 					<br />
// 					Full-time (40 hours).
// 				</p>
// 				<p>
// 					<Underline>Jun 2012 - Ongoing...</Underline>
// 					Owner sole proprietorship, FuturePortal.
// 					<br />
// 					Working on projects next to my studies and jobs.
// 				</p>
// 				<p>
// 					<Underline>Sep 2013 - Jan 2014</Underline>
// 					Final internship for &apos;Hogeschool Utrecht&apos; at Eagerly Internet in Utrecht.
// 					<br />
// 					Full-time trainee as front- and backend developer.
// 				</p>
// 				<p>
// 					<Underline>Jun 2012 - Jul 2012</Underline>
// 					Temporary worker as PHP programmer at CN-IT.
// 					<br />
// 					Full-time.
// 				</p>
// 				<p>
// 					<Underline>Mei 2011 - Jun 2012</Underline>
// 					Lead Web Developer at the Dragon Media Group B.V. in Leusden.
// 					<br />
// 					Part-time next to my study &apos;HBO Informatica&apos;.
// 				</p>
// 				<p>
// 					<Underline>Augustus 2009 - Mei 2011</Underline>
// 					Web Developer at the Dragon Media Group B.V. in Leusden.
// 					<br />
// 					Multiple internships, in vacations, and next to school.
// 					<br />
// 					<em>Vacations and internships</em>: Full-time.
// 					<br />
// 					<em>Next to my study</em>: 1 day a week, 8 hours.
// 				</p>
// 				<p>
// 					<strong>Education:</strong>
// 					<br />
// 					<Underline>2011 - 2014</Underline>
// 					HBO Informatica - Hogeschool Utrecht.
// 					<br />
// 					Minor: Web &amp; multimedia.
// 					<br />
// 					Specialization: Advanced software architecture.
// 					<br />
// 					Passed with merit (average grade 8 out of 10).
// 					<br />
// 					Finished the course in 3 years instead of the regular 4 years.
// 				</p>
// 				<p>
// 					<Underline>2008 - 2011</Underline>
// 					Application development (level 4) - ROC Midden Nederland.
// 					<br />
// 					Finished the course in 2 and a half year instead of the regular 4 years.
// 				</p>
// 				<p>
// 					<Underline>2004 - 2008</Underline>
// 					VMBO-T (now MAVO) Corderius College.
// 				</p>
// 				<p>
// 					<strong>courses:</strong>
// 					<br />
// 					2003 (age 12): Ten Finger System typing.
// 				</p>
// 				<p>
// 					<strong>Computer skills:</strong>
// 					<br />
// 					Prog: PHP, NodeJS<Old>, C#, Java</Old>
// 					<br />
// 					Web: Javascript ES6, Typescript, React, Vue.js, Styled components, SASS<Old>, jQuery</Old>
// 					<br />
// 					PHP: Laravel, Symfony, WordPress, custom frameworks<Old>, Zend Framework 2</Old>
// 					<br />
// 					SQL: PostgreSQL, MySQL
// 					<br />
// 					Linux: Ubuntu, Debian, Alpine, Raspbian
// 					<br />
// 					Mobi: React native<Old>, Titanium (iOS/Android)</Old>
// 					<br />
// 					Design: Photoshop, Gimp
// 					<br />
// 					Tools: Docker, Webpack, Composer, Git(Hub/Lab), Kubernetes<Old>, Vagrant, SaltStack, Deployer</Old>
// 				</p>
// 				<p>
// 					<strong>Motivations:</strong>
// 					<br />
// 					Very diligent and studious in programming area.
// 					<br />I like to build strong architectural systems with a user friendly interaction in a nice
// 					layout. Done via Continuous Deployment in a well tested environment.
// 				</p>
// 				<p>
// 					<strong>Leisure:</strong>
// 					<br />
// 					More programming.
// 					<br />
// 					Graphical design.
// 					<br />
// 					Driving car / motorcycle.
// 					<br />
// 					Fitness / powerlifting.
// 				</p>
// 			</Container>
// 		</Section>
// 	);
// };

// export default Page;

export {};
