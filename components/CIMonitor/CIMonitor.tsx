import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import useStyle from 'components/Layout/useStyle';

import styles from './CIMonitor.module.scss';

const CIMonitor = (): ReactElement => {
	const style = useStyle(styles);
	const { className: animation, style: animationStyle } = useAnimation('fadeInDown', 'fadeOutDown');

	return (
		<div {...style('section')}>
			<div {...style('container')}>
				<iframe
					{...style('frame', animation)}
					src="https://ci.rick.nu?completed=1&avatars=0"
					style={animationStyle}
				/>
			</div>
		</div>
	);
};

export default CIMonitor;
