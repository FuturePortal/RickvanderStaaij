import React, { ReactElement, useEffect, useState } from 'react';

import moment, { Moment } from 'moment';

import useStyle from 'components/Layout/useStyle';
import Flip from './Flip';

import styles from './Countdown.module.scss';

const steps = {
	days: 60 * 60 * 24,
	hours: 60 * 60,
	minutes: 60,
	seconds: 1,
};

const getSecondsRemaining = (target: Moment, continueAfterGoal: boolean) => {
	const timeLeft = target.diff(moment(), 'seconds');

	if (timeLeft > 0) {
		return timeLeft;
	}

	return continueAfterGoal ? Math.abs(timeLeft) : 0;
};

type Props = {
	target: Moment;
	continueAfterGoal?: boolean;
};

const Countdown = ({ target, continueAfterGoal }: Props): ReactElement => {
	const [remainder, setRemainder] = useState<number>(0);
	const style = useStyle(styles);

	useEffect(() => {
		const updateTime = () => setRemainder(getSecondsRemaining(target, continueAfterGoal));

		const updateInterval = setInterval(() => updateTime(), 1000);
		updateTime();

		return () => clearInterval(updateInterval);
	}, []);

	let secondsLeft = remainder;
	let hadValue = false;

	return (
		<div {...style('clock')}>
			{Object.keys(steps).map((step, index) => {
				const pick = Math.floor(secondsLeft / steps[step]);

				if (pick > 0) {
					hadValue = true;
				}

				if (index < 3 && !hadValue) {
					return null;
				}

				secondsLeft -= steps[step] * pick;

				return <Flip key={step} value={pick} title={step} />;
			})}
		</div>
	);
};

export default Countdown;
