import React, { ReactElement, useEffect, useState } from 'react';

import useStyle from 'components/Layout/useStyle';

import styles from './Flip.module.scss';

type Props = {
	title: string;
	value: string | number;
};

const Flip = ({ title, value }: Props): ReactElement => {
	const [currentValue, setCurrentValue] = useState(value);
	const [previousValue, setPreviousValue] = useState(value);
	const style = useStyle(styles);

	useEffect(() => {
		setPreviousValue(currentValue);
		setCurrentValue(value);
	}, [value]);

	return (
		<div {...style('container')}>
			<div {...style('title')}>{title}</div>
			<div {...style('flip')} key={currentValue}>
				<div {...style('old-top')}>{previousValue}</div>
				<div {...style('new-top')}>{currentValue}</div>
				<div {...style('old-bottom')} data-value={previousValue} />
				<div {...style('new-bottom')} data-value={currentValue} />
			</div>
		</div>
	);
};

export default Flip;
