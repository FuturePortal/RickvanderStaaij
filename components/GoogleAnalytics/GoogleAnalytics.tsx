import React, { ReactElement, useEffect } from 'react';

import Head from 'next/head';
import { useRouter } from 'next/router';

const GA_TRACKING_ID = 'G-J4LEMW5YG5';

const GoogleAnalytics = (): ReactElement => {
	const router = useRouter();

	useEffect(() => {
		const logRouteChange = () => {
			try {
				gtag('set', 'page_path', window.location.pathname);
				gtag('event', 'page_view');

				console.log(`Page view ${window.location.pathname}.`);
			} catch (_error) {
				console.log('Page view submission failed.');
			}
		};

		router.events.on('routeChangeComplete', logRouteChange);

		return () => router.events.off('routeChangeComplete', logRouteChange);
	}, [router.events]);

	return (
		<Head>
			<script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}></script>
			<script
				dangerouslySetInnerHTML={{
					__html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', '${GA_TRACKING_ID}');`,
				}}
			/>
		</Head>
	);
};

export default GoogleAnalytics;
