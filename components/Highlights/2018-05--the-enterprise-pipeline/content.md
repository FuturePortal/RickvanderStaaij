# The enterprise pipeline

![kanban stickies](/images/creations/the-enterprise-pipeline.png)

The ultimate workflow with Agile Kanban, Review Applications en Continuous Deployment.

Nowadays, you want to be able to go to production as fast as possible. Without having to wait for other features or
fixes in a big release. Using 'the enterprise pipeline' allows you to do that, without giving in on testability.

## Kanban board

[ ![kanban board with pipelines](/images/creations/kanban-board-with-pipelines.png) ](/images/creations/kanban-board-with-pipelines.png)

### To do

All tickets that are ready to be picked up by the development team start in the 'to do' column of the Kanban board. All
tickets in this column are ordered by their priority. Tickets at the top of the 'to do' column should be picked up first
by the developers.

### In progress

When a developer starts working on a ticket, the ticket is assigned to them and moved to the 'in progress' column. The
new code is created on a developer's machine.

### Technical review

When a developer completes a ticket, the changed and/or improved code is submit towards the codebase in a separate
branch. This will trigger the 'review pipeline' which makes the changed code available in a demo environment, called a
'review application'. Another developer will check the new/improved code and will check if the review application is
working as expected.

### Testing

todo

### Acceptance

todo

### Done

todo

## Automated pipelines

[ ![the enterprise pipelines](/images/creations/enterprise-pipelines.png) ](/images/creations/enterprise-pipelines.png)

### Review pipeline

todo

### Master pipeline

todo
