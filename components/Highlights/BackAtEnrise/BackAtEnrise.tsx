import React, { ReactElement } from 'react';

import moment from 'moment';
import Link from 'next/link';

import Image from 'components/Image';
import useInternalLink from '../../Layout/Go/useInternalLink';

import EnriseEntryImage from './enrise-entree.jpg';
import EnriseQuoteImage from './enrise-quote-wall.jpg';
import SignContractImage from './sign-contract.jpg';
import { Highlight } from 'types/highlight';

const BackAtEnrise = (): ReactElement => {
	const contactHref = useInternalLink(`/contact`);

	return (
		<>
			<p>
				<strong>
					Na 8 jaar met plezier werken bij Enrise ging ik op zoek naar een andere uitdaging, maar na anderhalf
					jaar kwam ik toch weer terug?! Hoe is dat gelopen?
				</strong>
			</p>
			<h2>Introductie</h2>
			<p>
				Na mijn afstuderen was ik niet zeker van in welke vorm ik wilde gaan werken. Blijf ik freelancen (wat ik
				al deed naast mijn studie), of ga ik bij een bedrijf werken? Dit is waar Enrise op mijn pad kwam. Een
				bedrijf waar de medewerkers veel zeggenschap hebben over het reilen en zeilen van de organisatie (een
				collectief van zelfsturende teams). Een goede combinatie tussen een vast dienstverband, en toch een stuk
				vrijheid houden in je werk. Dit voelde voor mij als de juiste match. Ik tekende bij Enrise.
			</p>
			<h2>Werken bij Enrise</h2>
			<p>
				Werken bij Enrise is altijd een mooie combinatie geweest van ontwikkelingswerkzaamheden en meedenken met
				de rest van de organisatie.
			</p>
			<p>
				Op het gebied van ontwikkeling, leerde ik elke dag wel iets nieuws. Ik hou heel erg van goede
				implementatie discussies en nadenken over hoe we mooie, onderhoudbare code schrijven. Zo is het mogelijk
				om voor grote klanten te werken. Maatwerk opdrachten waar we in staat zijn om zo goed mogelijk aan te
				sluiten op de wensen. Gebruik makend van nieuwe en/of moderne technieken. Blijven zoeken naar de beste
				en mooiste oplossing hield mij scherp.
			</p>
			<p>
				Dan de andere kant, namelijk de bedrijfsvoering. Ik was in meerdere groepjes actief die zich
				bezighielden met de organisatie van Enrise. Nadenken over lopende uitdagingen voor het bedrijf,
				onderdeel zijn van vaste processen, hoe we die kunnen verbeteren en wat voor leuke activiteiten we met
				elkaar kunnen ondernemen. Zeker toen Enrise de transitie naar een volledig zelfsturende organisatie ging
				maken (dus geen enkele vorm van management meer), was ik zeer betrokken bij dit proces.
			</p>
			<p>
				<Image source={EnriseEntryImage} alt="Enrise office front view" />
			</p>
			<h2>Waarom weggaan?</h2>
			<p>
				Het nadeel van een projectorganisatie is dat de werkvoorraad ups en downs heeft. Soms is er werk in
				overvloed en soms moeten we op zoek naar nieuwe klussen en richten we ons meer op doorontwikkeling en
				onderhoud. In zo’n rustigere periode deed ik mijn uiterste best om zo nuttig mogelijk bezig te blijven.
				Meegaan met klantgesprekken voor nieuwe business opportunities, bijdragen aan de uitingen van Enrise,
				achterstallig onderhoud oppakken en processen verbeteren.
			</p>
			<p>
				Het vele context switchen en steeds op zoek naar hoe je je zo nuttig mogelijk kan maken, kostte mij veel
				energie. Ik focus mij liever op een groot project, waar ik bij kan dragen om met mijn team het beste
				resultaat te halen.
			</p>
			<p>
				Na een tweede langere periode van werken zonder vaste focus begon ik mij af te vragen... Hoe zou het
				zijn in een product organisatie? Een team dat zich echt kan focussen op zijn eigen product, waar de tijd
				en ruimte is om processen te optimaliseren. Na 8 jaar bij Enrise heb ik dan ook besloten om mijn horizon
				te gaan verbreden en om te kijken hoe een ander bedrijf dingen aanpakt.
			</p>
			<h2>Andere bedrijven</h2>
			<p>
				Uiteindelijk heb ik bij twee andere bedrijven een kijkje in de keuken genomen, maar daar ben ik tot de
				conclusie gekomen dat het gras zeker niet groener was aan de overkant. Een paar van mijn bevindingen,
				die voor mij al heel vanzelfsprekend waren, maar toch in de praktijk helemaal niet goed worden
				uitgevoerd:
			</p>
			<ul>
				<li>
					<strong>Afgekaderde functies.</strong> Als je in een meer traditioneel bedrijf terechtkomt, wordt er
					al snel een specifiek label op je geplakt. In mijn geval was dit “frontend engineer”, iets waar ik
					zelf ook graag meer op wilde focussen. Maar het feit dat je heel veel breder ingezet kan worden,
					wordt dan al snel door anderen vergeten. Of je mening wordt niet altijd even serieus genomen. Ik heb
					het meedenken over alle linies erg gemist.
				</li>
				<li>
					<strong>Agile development.</strong> Ook al pretenderen de meeste bedrijven tegenwoordig “Agile” te
					zijn, écht Agile werken snappen ze (vaak) niet. Het gaat namelijk niet om de processen die er bij
					komen kijken, maar om de achterliggende waarden. Het kost tijd om dit bij iedereen goed te laten
					landen. Je kunt hierbij niet als bedrijf over één nacht ijs. Het verkopen van een Agile project is
					moeilijk, hiervoor moet je eerst vertrouwen winnen door te laten zien dat je de meeste waarde gaat
					leveren voor wat de klant denkt te willen hebben. Dit kan je niet van tevoren vangen in één vaste
					offerte. Toch gebeurt dit nog veel vaker dan je denkt, en dat zit het ontwikkeltraject in de weg. De
					focus op kwaliteit en waarde leveren valt dan in het niet met simpelweg functionaliteit bouwen.
				</li>
				<li>
					<strong>Niet klaar voor verandering.</strong> Als een bedrijf streeft naar een meer open
					organisatiestructuur, betekent dat dit op veel fronten waarschijnlijk nog niet het geval is. De
					grote uitdaging is om de hele organisatie mee te krijgen in zo’n transitie. En dat heeft tijd nodig.
				</li>
			</ul>
			<p>
				Bovenstaande punten vatten voor een groot deel samen waar ik tegenaan liep en waar ik te weinig ruimte
				kreeg om bij te dragen aan verbetering. Of simpelweg tegen dichte deuren aan liep. Een heel ander beeld
				dan mij was geschetst.
			</p>
			<h2>Terug? Of...</h2>
			<p>
				In retrospect, Enrise deed heel veel dingen heel erg goed. Een plek waar ik mij in mijn kracht voelde,
				waar ik kon leren van minstens even slimme collega’s. En waar altijd de ruimte was om te innoveren en
				dingen anders aan te pakken. Is Enrise dan het enige bedrijf dat het écht snapt? Vast niet… Maar dan is
				de vraag, wil ik nog een keer een gok wagen?
			</p>
			<p>
				Ga ik nog eens op zoek naar een bedrijf waar de vrijheid en verantwoordelijkheid echt bij de werknemers
				ligt? Het probleem is dat heel veel werkgevers dit beeld schetsen, en dat veel rooskleuriger doen
				overkomen tijdens de sollicitatie. Ik had immers bij de vorige sollicitaties ook echt goed alles
				uitgevraagd wat ik belangrijk vind in een organisatie. Dit proces nog eens starten, met de kans dat het
				dan toch tegenvalt, is een groot risico.
			</p>
			<p>
				Ik heb altijd goed contact gehouden met Enrise, en ik wist dat daar altijd weer plek voor mij zou zijn.
				Na een paar zeer fijne gesprekken met meerdere teams binnen Enrise te hebben gevoerd was het voor mij
				duidelijk. Van Enrise word ik blij. Dat is mij goud waard. Ik ga terug naar Enrise.
			</p>
			<p>
				<Image source={SignContractImage} alt="Signing my new contract at Enrise" />
			</p>
			<h2>Was het de juiste beslissing?</h2>
			<p>
				Inmiddels werk ik alweer een dik half jaar bij Enrise. En gelijk de eerste week was ik betrokken bij
				project inhoudelijke discussies die uitgingen van het beste resultaat voor onze klanten. Mét een
				oplossing waar wij blij worden en volledig achter kunnen staan. Ik was zo blij om weer mee te kunnen
				kiezen. Dit ging om projectwerkzaamheden, maar dezelfde ervaring had ik met het team en de rest van de
				organisatie. Ik voel me betrokken, in mijn kracht gezet, ik kan het verschil maken. Voor mijzelf, mijn
				collega’s en de klanten van Enrise. We sturen samen, om tot het mooiste resultaat te komen.
			</p>
			<p>
				<Image source={EnriseQuoteImage} alt="To Enrise, take (someone or something) up to greater hights" />
			</p>
			<p>
				Maar hoe zit het dan met de periodes dat er minder werkvoorraad is en ik meer context moet switchen om
				de meeste waarde te kunnen blijven leveren? Deze periodes kan ik nu veel beter voor lief nemen. Wetende
				wat ik er voor terug krijg in vergelijking met andere bedrijven. Plus, ik los een huidige, rustigere
				periode nu op door aan te haken bij een groot project van een ander team.
			</p>
			<p>
				Ik voel mij weer thuis. Wil je nog meer weten over mijn verhaal? Stel je vraag via{' '}
				<a {...contactHref}>het contactformulier</a>. En neem ook eens een kijkje op de website van{' '}
				<Link href="https://enrise.com" target="_blank">
					Enrise
				</Link>
				!
			</p>
		</>
	);
};

const item: Highlight = {
	title: 'Waarom ik ging werken bij Enrise, wegging en weer terugkwam!',
	slug: 'back-at-enrise',
	date: moment('2024-05-22'),
	intro:
		'🇳🇱 Na 8 jaar met plezier werken bij Enrise ging ik op zoek naar een andere uitdaging,' +
		' maar na anderhalf jaar kwam ik toch weer terug?! Hoe is dat gelopen?',
	content: <BackAtEnrise />,
	cover: SignContractImage,
};

export default item;
