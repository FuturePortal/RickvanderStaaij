import React, { ReactElement } from 'react';

import moment from 'moment';

import Image from 'components/Image';

import RSImage from './bot-scripter.png';
import PlayerFormImage from './player-form.png';
import SRLForumsAccountImage from './SRL-forums-account.gif';
import { Highlight } from 'types/highlight';

const BotScripter = (): ReactElement => (
	<>
		<p>
			Back in the day, I played RuneScape a lot. I noticed some players in the game that didn&apos;t seem to be
			actual players. More like robots that play the game for you. Later I figured there are programs that allow
			you to write scripts based on screen inputs (like colors and patterns), that then can control the mouse and
			keyboard for you. I was intrigued by how this was working, and I started to research it. Eventually I
			started writing my own scripts.
		</p>
		<p>
			It started simple, but I excelled quite quickly. I&apos;ve made multiple scripts for various tasks (if
			you&apos;re familiar: bone pick & bury, Varrock iron miner, Pest Control). And I&apos;ve created easy setup
			forms for those scripts too:
		</p>
		<p>
			<Image
				source={PlayerFormImage}
				alt="Player setup form to start the bot"
				description="Preview of the player setup form to run the script"
				width="30rem"
			/>
		</p>
		<p>
			All scripts I wrote were posted on an open source platform (SRL forums), where other people could download
			and run the script I&apos;ve had made. My most popular script had a total runtime of over 3 years by other
			people, amazing.
		</p>
		<p>
			Because my scripts were quite known and popular on the forum, I was awarded with a &quot;SRL scripter&quot;
			rank. That was later replaced by the Einstein, making me a &quot;SRL master scripter&quot;. This was a
			really rare rank as only 30 of the 30.000 members of the forum had the Einstein, so you can imagine that I
			was really proud of achieving that!
		</p>
		<p>
			<Image
				source={SRLForumsAccountImage}
				alt="SRL forums profile and awards"
				description="SRL forums profile and awards"
				width="20rem"
			/>
		</p>
		<p>
			I always saw the RuneScape scripts as the start of my career as a programmer. I learned a lot, even so much
			that IT school got pretty embarrassing... But thanks to RuneScape I&apos;m the programmer that I am today!
		</p>
	</>
);

const item: Highlight = {
	title: 'Runescape bot master scripter',
	slug: 'bot-scripter',
	date: moment('2007-05'),
	intro: 'I started my career creating RuneScape scripts. Scripts that automatically play the game for you.',
	content: <BotScripter />,
	cover: RSImage,
};

export default item;
