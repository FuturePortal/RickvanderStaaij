import React, { ReactElement } from 'react';

import moment from 'moment';
import Link from 'next/link';

import Image from 'components/Image';
import { Highlight } from 'types/highlight';

import CIMonitorEpicImage from './cimonitor.png';
import CIStatusImage from './cimonitor-job-running.gif';
import MarbleRunImage from './marble-run.gif';

const CIMonitor = (): ReactElement => (
	<>
		<p>
			<Image
				source={CIMonitorEpicImage}
				alt="CIMonitor in the EPIC space"
				description="CIMonitor in the EPIC space"
			/>
		</p>
		<p>
			In the past, deployments (going live with new products/updates) were scary, and lots could go wrong.
			Nowadays almost every aspect of the deployment process is automated. Automated to the point were you
			wouldn&apos;t event notice anything of the progress it&apos;s making. Is my code deployed to production
			already? With CIMonitor you can easily setup a CI status dashboard to track the progress of your automated
			processes.
		</p>
		<p>
			<Image
				source={CIStatusImage}
				alt="CIMonitor running status build"
				description="CIMonitor showing a running build"
			/>
		</p>
		<p>
			But it doesn&apos;t stop there. Where deploying new features/products to production was scary before, it
			should be considered a party now. And you preferably want that party to take place as many times as
			possible. But how do you do that if it&apos;s automated? Right, you automate that too!
		</p>
		<p>
			<Image source={MarbleRunImage} alt="Marble run module example" description="Marble run module example" />
		</p>
		<p>
			CIMonitor allows you to create events for specific actions. For example, when a new update of your
			application is pushed to production, you can:
		</p>
		<ul>
			<li>Trigger a marble run</li>
			<li>Play some victory music</li>
			<li>Have a beacon light flashing</li>
			<li>Have led strips representing the status of your CI</li>
		</ul>
		<p>
			And much more... The{' '}
			<a href="https://github.com/FuturePortal/CIMonitor" target="_blank" rel="noreferrer">
				CIMonitor project is open-source on GitHub
			</a>
			, and everyone can contribute. I&apos;m very proud to be the creator of the project, and I hope many
			development organizations will adopt the project into their development flow.
		</p>
		<p>
			I&apos;ve made <Link href="/ci">CIMonitor part of my website</Link>, to show you the insight it can give.
			Check it out!
		</p>
	</>
);

const item: Highlight = {
	title: 'CIMonitor, a status monitor for your CI',
	slug: 'cimonitor',
	date: moment('2018-11'),
	intro: 'Nowadays almost every aspect of the deployment process is automated. It should be considered a party!',
	content: <CIMonitor />,
	cover: CIMonitorEpicImage,
};

export default item;
