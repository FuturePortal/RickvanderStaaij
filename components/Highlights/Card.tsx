import React, { Fragment, ReactElement } from 'react';

import Image from 'next/image';

import useAnimation from 'components/Animation';
import useInternalLink from 'components/Layout/Go/useInternalLink';
import useStyle from 'components/Layout/useStyle';
import { Highlight } from 'types/highlight';

import styles from './HighlightCard.module.scss';

type Props = {
	highlight: Highlight;
	index: number;
};

const HighlightCard = ({ highlight, index }: Props): ReactElement => {
	const href = useInternalLink(`/highlight/${highlight.slug}`);
	const style = useStyle(styles);
	const { className: animation, style: animationStyle } = useAnimation(
		'fadeInRight',
		'fadeOutLeft',
		200 + index * 200
	);

	return (
		<Fragment key={index}>
			<a {...style('card', animation)} style={animationStyle} {...href}>
				<Image src={highlight.cover} alt={highlight.title} />
				<div {...style('content')}>
					<h2>{highlight.title}</h2>
					<span>{highlight.date.format('MMMM Y')}</span>
					<p>{highlight.intro}</p>
				</div>
			</a>
		</Fragment>
	);
};

export default HighlightCard;
