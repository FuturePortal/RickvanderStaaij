import React, { ReactElement } from 'react';

import moment from 'moment';

import Image from 'components/Image';

import ProductOverview from './biketotaal-productpage.gif';
import BikeTotaalImage from './biketotaal-responsive.png';
import FrontendLayers from './frontend-layers.png';
import ProfileImage from './profile-responsive.png';
import { Highlight } from 'types/highlight';

const Dynamo = (): ReactElement => (
	<>
		<p>
			In 2020, a new project opportunity came to Enrise. The Dynamo Retail Group was looking for a web development
			company that could help them bringing their shattered platform together to a single, stable and performing
			platform. I was one of the main developers that joined the project to realise this.
		</p>
		<p>
			<Image
				source={ProductOverview}
				alt="BikeTotaal product detail page"
				description="The BikeTotaal product detail page"
			/>
		</p>
		<p>
			Since Dynamo had multiple of their websites being built by multiple web development companies, we were
			challenged to bring their shops together into a single platform. We&apos;ve had decided to work with{' '}
			<strong>Shopware</strong> as our backend. For our frontend application we worked with{' '}
			<strong>Next.js</strong>, which is built with <strong>React and Typescript</strong>.
		</p>
		<p>
			As shown in the image below, we made sure all of the web shops were served from the same application, with a
			separate theme for each specific shop. This was done with <strong>styled component themes</strong>.
		</p>
		<p>
			<Image
				source={FrontendLayers}
				alt="Frontend architecture layers"
				description="The architecture layers of the Dynamo frontend"
			/>
		</p>
		<p>Even though the websites look quite similar, they&apos;re quite different at the same time too!</p>
		<p>
			<Image
				source={ProfileImage}
				alt="Dynamo running status builds"
				description="The responsive 'Profile de fietsspecialist' webiste"
			/>
		</p>
		<p>
			These are the kind of projects I enjoy building most. A pretty website, with a strong architecture. I hope
			to do more projects like this soon!
		</p>
	</>
);

const item: Highlight = {
	title: 'Dynamo, the bike store rebuild',
	slug: 'dynamo-rebuild',
	date: moment('2021-04'),
	intro: `Bike Totaal, Profile 'de fietsspecialist', World of bikes and tweedehandsfietsen.nl in a new package.`,
	content: <Dynamo />,
	cover: BikeTotaalImage,
};

export default item;
