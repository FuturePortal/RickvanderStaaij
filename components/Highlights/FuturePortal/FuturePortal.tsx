import React, { ReactElement } from 'react';

import moment from 'moment';

import Image from 'components/Image';

import CardsImage from './futureportal-cards.png';
import { Highlight } from 'types/highlight';

const FuturePortal = (): ReactElement => (
	<>
		<p>
			<Image
				source={CardsImage}
				alt="FuturePortal business cards"
				description="FuturePortal business cards I designed"
			/>
		</p>
		<p>
			Next to my batchelor IT study, I&apos;ve had created my own company: FuturePortal. I used my company to work
			on some private projects, and do some work for small companies too.
		</p>
		<p>
			When I finished my bachelors, I&apos;ve had to make a decision. Continue for my own company, or find a
			full-time job. I discovered Enrise as a self-steering IT company which gave me enough freedom and a steady
			income. Nowadays FuturePortal is a name I cary with me for some private projects, but nothing publicly
			anymore.
		</p>
	</>
);

const item: Highlight = {
	title: 'FuturePortal web development',
	slug: 'futureportal',
	date: moment('2012-07'),
	intro: 'FuturePortal was my own company for side projects and some small hired projects next to my study.',
	content: <FuturePortal />,
	cover: CardsImage,
};

export default item;
