import React, { ReactElement } from 'react';

import moment from 'moment';

import useAnimation from 'components/Animation';
import useInternalLink from 'components/Layout/Go/useInternalLink';
import useStyle from 'components/Layout/useStyle';
import { Highlight } from 'types/highlight';

import styles from './Highlight.module.scss';

type Props = {
	highlight: Highlight;
};

const HighlightContent = ({ highlight }: Props): ReactElement => {
	const animation = useAnimation();
	const contentAnimation = useAnimation('fadeInUp', 'fadeOutDown');
	const href = useInternalLink('/highlights');
	const style = useStyle(styles);

	return (
		<div {...style('section')}>
			<div {...style('container')}>
				<div {...animation}>
					<p>
						<a {...href}>‹ back to highlights</a>
					</p>
					<hr />
					<h1>{highlight.title}</h1>
					<p {...style('date')}>{moment(highlight.date).format('MMMM Y')}</p>
					<hr />
				</div>
				<div {...contentAnimation}>
					{highlight.content}
					<hr />
					<p>
						<a {...href}>Check more of my highlights ›</a>
					</p>
				</div>
			</div>
		</div>
	);
};

export default HighlightContent;
