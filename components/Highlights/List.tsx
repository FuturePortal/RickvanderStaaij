import React, { ReactElement } from 'react';

import useStyle from 'components/Layout/useStyle';
import HighlightCard from './Card';
import { Highlight } from 'types/highlight';

import styles from './HighlightCard.module.scss';

type Props = {
	highlights: Highlight[];
};

const HighlightList = ({ highlights }: Props): ReactElement => {
	const style = useStyle(styles);

	return (
		<div {...style('list')}>
			{highlights.map((highlight, index) => (
				<HighlightCard key={highlight.slug} index={index} highlight={highlight} />
			))}
		</div>
	);
};

export default HighlightList;
