import React, { ReactElement } from 'react';

import moment from 'moment';

import Image from 'components/Image';

import NawcastHeaderImage from './nawcast.png';
import EditorDemoImage from './nawcast-demo.gif';
import RunnerDemoImage from './nawcast-runner-demo.gif';
import { Highlight } from 'types/highlight';

const Nawcast = (): ReactElement => (
	<>
		<p>
			<Image
				source={NawcastHeaderImage}
				alt="Nawcast start running image"
				description="Start running your narrowcastings screen"
			/>
		</p>
		<p>
			Narrowcasting is popular due to the fact that you can create content specifically for your customers.
			However, there is no system available that lets you create a narrowcasting without some home made components
			by a web development company. Simpel wishes to change that.
		</p>
		<p>
			<Image source={EditorDemoImage} alt="Nawcast editor demo" description="Nawcast editor demo" />
		</p>
		<p>
			When you&apos;ve created an account, you&apos;re able to compose a narrowcasting program using the available
			templates. In the demo above there are only two, but there should be a lot more in the future! Allowing you
			to easily and quickly create a narrowcasting that suits your customers.
		</p>
		<p>
			<Image source={RunnerDemoImage} alt="Nawcast runner demo" description="Nawcast runner demo" />
		</p>
		<p>
			When you&apos;ve created a program, all you need is any kind of display with a web browser. You get a unique
			start-code on your display, which can be entered in your dashboard. Simply select the program you want to
			run, and hit start! That&apos;s it!
		</p>
		<p>Simpel allows you to create a narrowcasting within minutes.</p>
		<p>
			This project is far from done, but I want to spend more time on it to make it a commercially viable product!
		</p>
	</>
);

const item: Highlight = {
	title: 'Nawcast, narrowcasting in minutes',
	slug: 'nawcast',
	date: moment('2018-06'),
	intro: 'Nawcast is a quick and easy platform to create your own narrowcasting in minutes.',
	content: <Nawcast />,
	cover: NawcastHeaderImage,
};

export default item;
