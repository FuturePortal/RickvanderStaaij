import React, { ReactElement } from 'react';

import moment from 'moment';

import Image from 'components/Image';

import PuurDichtbijProductScreen from './product-page.jpg';
import PuurDichtbijTruck from './puurdichtbij.png';
import { Highlight } from 'types/highlight';

const PuurDichtbij = (): ReactElement => (
	<>
		<p>
			When I joined{' '}
			<a href="https://enrise.com" target="_blank" rel="noreferrer">
				Enrise
			</a>
			, one of the first projects I&apos;ve worked on was PuurDichtbij. A project that would bring fresh stock
			directly from farmers to their customers. Without going via a regular supermarket for example.
		</p>
		<p>
			<Image
				source={PuurDichtbijTruck}
				alt="PuurDichtbij delivery truck"
				description="PuurDichtbij delivery truck"
			/>
		</p>
		<p>
			Because of this concept, they wanted to develop an app for both Android and iOS. Together with a small team
			we created the PuurDichtbij app:
		</p>
		<p>
			<Image
				source={PuurDichtbijProductScreen}
				alt="PuurDichtbij product screen"
				description="PuurDichtbij product screen"
				width="400px"
			/>
		</p>
		<p>I was really happy with the result, and so were PuurDichtij and their customers.</p>
	</>
);

const item: Highlight = {
	title: 'PuurDichtbij mobile app',
	slug: 'puurdichtbij',
	date: moment('2014-06'),
	intro: 'The PuurDichtbij app was created to order fresh products from local farmers.',
	content: <PuurDichtbij />,
	cover: PuurDichtbijTruck,
};

export default item;
