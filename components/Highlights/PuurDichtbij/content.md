# PuurDichtbij app

When I joined [Enrise](https://enrise.com), one of the first projects I've worked on was PuurDichtbij. A project that
would bring fresh stock directly from farmers to their customers. Without going via a regular supermarket for example.

![PuurDichtbij delivery truck](/images/creations/puurdichtbij.png)

Because of this concept, they wanted to develop an app for both Android and iOS. Together with a small team we created
the PuurDichtbij app:

![App product detail page](/images/creations/puurdichtbij/product-page.jpg)

I was really happy with the result, and so were PuurDichtij and their customers.
