import React, { ReactElement } from 'react';

import moment from 'moment';

import Image from 'components/Image';

import DashboardImage from './dashboard.jpg';
import HomepageGif from './homepage.gif';
import MobileImage from './simpel.png';
import { Highlight } from 'types/highlight';

const Simpel = (): ReactElement => (
	<>
		<p>
			<Image
				source={HomepageGif}
				alt="Simpel.nl homepage"
				description="How the homepage was looking when we did the initial release"
			/>
		</p>
		<p>
			One of the first bigger projects I started working on at Enrise was the Simpel.nl rebuild. The project
			started of as a simple front-end replacement. But in the end Enrise rebuilt their entire digital
			infrastructure.
		</p>
		<p>
			I&apos;ve played a big part in the Simpel.nl rebuild, mainly focussing on the integration between the
			front-end and the back-end. Making sure that the end-user has an as pleasant as possible experience.
		</p>
		<p>
			<Image
				source={MobileImage}
				alt="Simpel.nl dashboard on mobile phone"
				description="The 'Mijn Simpel' dashboard on a mobile phone"
			/>
		</p>
		<p>
			Since I was so conviced of the quality that the team delivered, I was one of the first 20 customers in the
			new system. And I still am. I&apos;ve worked with the Simpel project for the first 3 years of its rebuid.
			After that I continued with other projects within Enrise. But I&apos;m still very proud of the result and
			the role I&apos;ve played in the project.
		</p>
		<p>
			<Image
				source={DashboardImage}
				alt="Simpel.nl self service dashboard"
				description="How the 'Mijn Simpel' dashboard was looking when we released it"
			/>
		</p>
	</>
);

const item: Highlight = {
	title: 'Simpel.nl greenfield rebuild',
	slug: 'simpel',
	date: moment('2016-06'),
	intro: 'The big simpel.nl mobile service provider greenfield project.',
	content: <Simpel />,
	cover: MobileImage,
};

export default item;
