import sortByDate from 'helpers/SortByDate';
import BackAtEnrise from './BackAtEnrise';
import BotScripter from './BotScripter';
import CIMonitor from './CIMonitor';
import Dynamo from './Dynamo';
import FuturePortal from './FuturePortal';
import Nawcast from './Nawcast';
import PuurDichtbij from './PuurDichtbij';
import Simpel from './Simpel';
import { Highlight } from 'types/highlight';

const highlights: Highlight[] = [
	Dynamo,
	CIMonitor,
	Nawcast,
	BotScripter,
	Simpel,
	FuturePortal,
	PuurDichtbij,
	BackAtEnrise,
].sort(sortByDate);

export default highlights;
