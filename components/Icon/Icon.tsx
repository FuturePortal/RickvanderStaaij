import { ReactElement } from 'react';

type Props = {
	icon: string;
	title?: string;
};

const Icon = ({ icon, title }: Props): ReactElement => {
	const classes = ['icon'];

	if (['autorenew'].includes(icon)) {
		classes.push('spin');
	}

	return (
		<span className={classes.join(' ')} title={title || ''}>
			{icon}
		</span>
	);
};

export default Icon;
