import React, { ReactElement } from 'react';

import NextImage, { StaticImageData } from 'next/image';

import useStyle from 'components/Layout/useStyle';

import styles from './Image.module.scss';

type Props = {
	source: StaticImageData;
	alt: string;
	description?: string;
	width?: string;
};

const Image = ({ source, alt, description, width }: Props): ReactElement => {
	const style = useStyle(styles);

	return (
		<span {...style('wrapper')} style={{ width }}>
			<NextImage src={source} alt={alt} />
			{description && <span {...style('description')}>{description}</span>}
		</span>
	);
};

export default Image;
