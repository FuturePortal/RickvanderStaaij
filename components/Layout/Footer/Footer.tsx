import React, { ReactElement } from 'react';

import useStyle from '../useStyle';
import links from './links';

import styles from './Footer.module.scss';

const Footer = (): ReactElement => {
	const style = useStyle(styles);

	return (
		<div {...style('bar', 'animate__animated', 'animate__fadeInUp')}>
			<a rel="noreferrer" href="https://gitlab.com/FuturePortal/RickvanderStaaij/" target="_blank">
				Rick van der Staaij &copy; {new Date().getFullYear()}
			</a>
			<div {...style('options')}>
				{links.map((link, index) => (
					<a
						{...style('option')}
						key={index}
						href={link.url}
						title={link.name}
						target="_blank"
						rel="noreferrer"
					>
						<div {...style('icon')}>
							<link.icon />
						</div>
					</a>
				))}
			</div>
			{/* <GlassBar position="top" /> */}
		</div>
	);
};

export default Footer;
