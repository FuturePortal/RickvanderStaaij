import GitHub from './icon/github.svg';
import GitLab from './icon/gitlab.svg';
import Instagram from './icon/instagram.svg';
import Linkedin from './icon/linkedin.svg';

type Link = {
	url: string;
	icon: string;
	name: string;
};

const links: Link[] = [
	{
		name: 'Linkedin',
		url: 'https://www.linkedin.com/in/rickvanderstaaij/',
		icon: Linkedin,
	},
	{
		name: 'Instagram',
		url: 'https://www.instagram.com/rick.nu/',
		icon: Instagram,
	},
	{
		name: 'GitHub',
		url: 'https://github.com/rick-nu',
		icon: GitHub,
	},
	{
		name: 'GitLab',
		url: 'https://gitlab.com/rick.nu',
		icon: GitLab,
	},
];

export default links;
