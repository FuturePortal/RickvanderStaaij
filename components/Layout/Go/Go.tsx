import React, { ReactElement, ReactNode } from 'react';

import Link from 'next/link';

type Props = {
	to: string;
	children: ReactNode;
};

const Go = ({ to, children }: Props): ReactElement => (
	<Link href={to} passHref>
		{children}
	</Link>
);

export default Go;
