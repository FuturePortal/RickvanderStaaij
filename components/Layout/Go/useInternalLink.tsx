import React from 'react';

import { useRouter } from 'next/router';

type AnimationProps = {
	href: string;
	onClick: (event: React.MouseEvent<HTMLElement>) => void;
};

const useInternalLink = (target: string): AnimationProps => {
	const router = useRouter();

	return {
		href: target,
		onClick: (event: React.MouseEvent<HTMLElement>) => {
			event.preventDefault();

			if (router.asPath === target) {
				return;
			}

			const outroEvent = new Event('page-outro');

			window.dispatchEvent(outroEvent);

			setTimeout(() => {
				router.push(target);
			}, 800);
		},
	};
};

export default useInternalLink;
