import React, { ReactElement } from 'react';

import useStyle from 'components/Layout/useStyle';
import { at, rand } from './helpers';
import useCanvas from './useCanvas';

import styles from './BackgroundEffects.module.scss';

type Particle = {
	created: number;
	duration: number;
	startX: number;
	startY: number;
	changeX: number;
	changeY: number;
	size: number;
	red: number;
	blue: number;
	green: number;
};

const drawParticle = (canvas: CanvasRenderingContext2D, particle: Particle, now: number) => {
	const completion = (now - particle.created) / particle.duration;
	const atOpacity = at(1, -1, completion);

	canvas.fillStyle = `rgba(${particle.red}, ${particle.blue}, ${particle.green}, ${atOpacity})`;
	canvas.beginPath();
	canvas.arc(
		at(particle.startX, particle.changeX, completion),
		at(particle.startY, particle.changeY, completion, 'easeInQuad'),
		atOpacity * particle.size,
		0,
		360
	);
	canvas.fill();
};

let particles: Particle[] = [];
let latest = 0;

const draw = (canvas: CanvasRenderingContext2D) => {
	const now = new Date().getTime();
	const drawWidth = 460;
	const drawHeight = 192;

	canvas.clearRect(0, 0, drawWidth, drawHeight);

	particles = particles.filter((particle) => now <= particle.created + particle.duration);

	if (now - latest > 300) {
		latest = now;
		const size = Math.round(20 + Math.random() * 50);
		particles.push({
			created: now,
			duration: Math.round(2000 + Math.random() * 3000),
			startX: 230,
			startY: Math.round(-size),
			changeX: Math.round(0 - drawWidth / 2 + Math.random() * drawWidth),
			changeY: Math.round(130 + Math.random() * 400),
			size: size,
			red: rand(140, 160),
			blue: rand(120, 140),
			green: rand(50, 70),
		});
	}

	particles.forEach((particle) => drawParticle(canvas, particle, now));
};

const BackgroundEffects = (): ReactElement => {
	const canvasRef = useCanvas(draw);
	const style = useStyle(styles);

	return (
		<div {...style('wrapper')}>
			<canvas {...style('canvas')} width={460} height={192} ref={canvasRef} />
		</div>
	);
};

export default BackgroundEffects;
