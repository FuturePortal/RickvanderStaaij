export const easings = {
	linear: (t: number): number => t,
	easeInQuad: (t: number): number => t * t,
};

export const at = (start: number, change: number, completion: number, ease = 'linear'): number => {
	completion = easings[ease](completion);
	return start + completion * change;
};

export const rand = (start: number, end: number): number => Math.round(start + Math.random() * (end - start));
