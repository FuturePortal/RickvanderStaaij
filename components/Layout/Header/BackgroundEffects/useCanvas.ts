import { MutableRefObject, useEffect, useRef } from 'react';

const useCanvas = (draw: (context: CanvasRenderingContext2D) => void): MutableRefObject<HTMLCanvasElement> => {
	const canvasRef = useRef(null);

	useEffect(() => {
		const canvas: HTMLCanvasElement = canvasRef.current;
		const context = canvas.getContext('2d');
		let animationFrameId;

		const render = () => {
			draw(context);
			animationFrameId = window.requestAnimationFrame(render);
		};
		render();

		return () => {
			window.cancelAnimationFrame(animationFrameId);
		};
	}, [draw]);

	return canvasRef;
};

export default useCanvas;
