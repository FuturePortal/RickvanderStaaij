import React, { ReactElement, useEffect, useState } from 'react';

import useStyle from 'components/Layout/useStyle';

import styles from './DynamicTitle.module.scss';

const titles = [
	'Senior software engineer',
	'Full-stack web developer',
	'Amateur powerlifter',
	'Frontend lover',
	'DevOps enthusiast',
	'Petrol head',
];

const timeout = (miliseconds: number): Promise<void> => {
	return new Promise((resolve) => setTimeout(resolve, miliseconds));
};

const DynamicTitle = (): ReactElement => {
	const [isRunning, setRunning] = useState<boolean>(true);
	const [currentTitle, setTitle] = useState<string>(titles[0]);
	const style = useStyle(styles);

	const typeTitle = async (title: string) => {
		if (!isRunning) {
			return;
		}

		for (let index = 1; index <= title.length; index++) {
			setTitle(title.substring(0, index) + '_');

			await timeout(20 + Math.round(Math.random() * 100));
		}

		await timeout(3000);

		for (let index = 1; index <= title.length; index++) {
			setTitle(title.substring(0, title.length - index) + '_');

			await timeout(20);
		}
	};

	const typeTitles = async () => {
		if (!isRunning) {
			return;
		}

		for (const title of titles) {
			await typeTitle(title);
		}

		typeTitles();
	};

	useEffect(() => {
		typeTitles();

		return () => setRunning(false);
	}, []);

	return <div {...style('title')}>{currentTitle}</div>;
};

export default DynamicTitle;
