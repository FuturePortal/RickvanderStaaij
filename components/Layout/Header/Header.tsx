import React, { ReactElement } from 'react';

import Image from 'next/image';

import useInternalLink from 'components/Layout/Go/useInternalLink';
import BackgroundEffects from 'components/Layout/Header/BackgroundEffects';
import DynamicTitle from 'components/Layout/Header/DynamicTitle';
import useStyle from '../useStyle';

import RickFaceImage from './rick-render.png';

import styles from './Header.module.scss';

const Header = (): ReactElement => {
	const style = useStyle(styles);

	return (
		<div {...style('wrapper', 'animate__animated', 'animate__fadeInDown')}>
			<div {...style('content')}>
				<BackgroundEffects />
				<a {...style('headshot')} {...useInternalLink('/')}>
					<Image src={RickFaceImage} alt="Rick van der Staaij" priority />
				</a>
				<a {...style('title-block')} {...useInternalLink('/')}>
					<h1 {...style('name')}>Rick van der Staaij</h1>
					<DynamicTitle />
				</a>
			</div>
		</div>
	);
};

export default Header;
