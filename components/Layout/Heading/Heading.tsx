import React, { ReactElement, ReactNode } from 'react';

import useInternalLink from 'components/Layout/Go/useInternalLink';
import useStyle from 'components/Layout/useStyle';

import styles from './Heading.module.scss';

type Props = {
	title: string;
	url?: string;
};

const Heading = ({ title, url }: Props): ReactElement => {
	const href = useInternalLink(url);
	const style = useStyle(styles);

	const getTitle = (): ReactNode => {
		if (url) {
			return <a {...href}>{title}</a>;
		}

		return title;
	};

	return (
		<span {...style('wrapper')}>
			<span {...style('bar')} />
			<h2 {...style('title')}>{getTitle()}</h2>
			<span {...style('bar')} />
		</span>
	);
};

export default Heading;
