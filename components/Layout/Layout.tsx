import React, { ReactElement, ReactNode } from 'react';

import Footer from './Footer';
import Header from './Header';
import Nav from './Nav';
import useStyle from './useStyle';

import styles from './Layout.module.scss';

type Props = {
	children: ReactNode;
};

const Layout = ({ children }: Props): ReactElement => {
	const style = useStyle(styles);

	return (
		<div {...style('wrapper')}>
			<Header />
			<Nav />
			<div {...style('content')}>{children}</div>
			<Footer />
		</div>
	);
};

export default Layout;
