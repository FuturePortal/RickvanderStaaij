import React, { ReactElement } from 'react';

import useStyle from '../useStyle';

import styles from './MenuToggle.module.scss';

type Props = {
	onToggle: () => void;
	open: boolean;
};

const MenuToggle = ({ onToggle, open }: Props): ReactElement => {
	const style = useStyle(styles);

	return (
		<button {...style('button')} type="button" onClick={onToggle}>
			<div {...style('text')}>Menu</div>
			<div {...style('toggle', open ? 'open' : null)} />
		</button>
	);
};

export default MenuToggle;
