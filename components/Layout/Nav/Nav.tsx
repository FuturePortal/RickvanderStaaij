import React, { ReactElement, useEffect, useState } from 'react';

import { useRouter } from 'next/router';

import AnimationDelay from 'components/Animation/AnimationDelay';
import useInternalLink from 'components/Layout/Go/useInternalLink';
import MenuToggle from 'components/Layout/MenuToggle';
import useStyle from '../useStyle';
import links from './links';

import styles from './Nav.module.scss';

type ItemProps = {
	name: string;
	target: string;
};

const Item = ({ target, name }: ItemProps) => {
	const router = useRouter();
	const href = useInternalLink(target);
	const style = useStyle(styles);
	const isCurrentRoute = router.pathname === target;

	return (
		<a {...style('route', isCurrentRoute ? 'route--current' : null)} {...href}>
			{name}
		</a>
	);
};

const Nav = (): ReactElement => {
	const router = useRouter();
	const style = useStyle(styles);
	const [menuOpen, setMenuOpen] = useState<boolean>(false);
	const [isSticky, setSticky] = useState<boolean>(false);

	const closeMenuAndClearAnimationDelay = () => {
		setMenuOpen(false);
		AnimationDelay.clearDelay();
	};

	const onScroll = (event) => {
		const scrollTop = event.target.documentElement?.scrollTop ?? 0;
		const sticky = scrollTop > 12 * 16;
		setSticky(sticky);
	};

	useEffect(() => {
		router.events.on('routeChangeStart', closeMenuAndClearAnimationDelay);
		window.addEventListener('page-outro', closeMenuAndClearAnimationDelay);
		window.addEventListener('scroll', onScroll);

		return () => {
			router.events.off('routeChangeStart', closeMenuAndClearAnimationDelay);
			window.removeEventListener('page-outro', closeMenuAndClearAnimationDelay);
			window.removeEventListener('scroll', onScroll);
		};
	}, []);

	return (
		<>
			<div {...style('bar', isSticky ? 'bar--sticky' : null, 'animate__animated', 'animate__fadeInDown')}>
				<div {...style('menu', 'container')}>
					<div {...style('brand-block')}>
						<a {...style('brand')} {...useInternalLink('/')}>
							Rick van der Staaij
						</a>
						<div {...style('mobile-toggle')}>
							<MenuToggle onToggle={() => setMenuOpen(!menuOpen)} open={menuOpen} />
						</div>
					</div>
					<div {...style('routes', menuOpen ? 'routes--open' : null)}>
						{links.map(({ uri, name }) => (
							<Item name={name} target={uri} key={uri} />
						))}
					</div>
				</div>
			</div>
			<div {...style('sticky-space')} />
		</>
	);
};

export default Nav;
