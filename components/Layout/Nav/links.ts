type Page = {
	name: string;
	uri: string;
};

const links: Page[] = [
	{
		name: 'About',
		uri: '/',
	},
	{
		name: 'Career',
		uri: '/career',
	},
	{
		name: 'Highlights',
		uri: '/highlights',
	},
	{
		name: 'Recommendations',
		uri: '/recommendations',
	},
	{
		name: 'Events',
		uri: '/events',
	},
	{
		name: 'Site specifications',
		uri: '/site-specifications',
	},
	{
		name: 'CIMonitor',
		uri: '/ci',
	},
	{
		name: 'Contact',
		uri: '/contact',
	},
];

export default links;
