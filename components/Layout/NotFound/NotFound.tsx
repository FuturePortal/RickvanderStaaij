import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import PageMeta from 'components/Layout/PageMeta';
import useStyle from '../useStyle';

const NotFound = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInUp', 'fadeOutDown');
	const style = useStyle();

	return (
		<div {...style('section', animation)} style={animationStyle}>
			<div {...style('container')}>
				<PageMeta title="Not found" description="Page or file not found." />
				<h1>Yeah, no</h1>
				<p>This doesn&apos;t exist... Use the navigation to get out of here.</p>
			</div>
		</div>
	);
};

export default NotFound;
