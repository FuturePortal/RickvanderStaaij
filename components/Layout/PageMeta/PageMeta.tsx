import React, { ReactElement } from 'react';

import Head from 'next/head';

type Props = {
	title: string;
	description: string;
};

const PageMeta = ({ title, description }: Props): ReactElement => (
	<Head>
		<title>{`${title} • Rick van der Staaij`}</title>
		<meta name="description" content={description} />
		<meta
			property="og:image"
			content={`${process.env.NEXT_PUBLIC_APP_URL}/api/page-image?title=${encodeURI(title)}`}
		/>
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
		<link rel="manifest" href="/site.webmanifest" />
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#343a40" />
		<meta name="msapplication-TileColor" content="#ffffff" />
		<meta name="theme-color" content="#252525" />
	</Head>
);

export default PageMeta;
