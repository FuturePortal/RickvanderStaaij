type StylesFile = {
	readonly [key: string]: string;
};

type UseClassResult = {
	className: string;
};

type UseStyle = (className: string, ...additionalClasses: string[]) => UseClassResult;

const useStyle = (styleFile: StylesFile = null): UseStyle => {
	const useClass = (className: string, ...additionalClasses: string[]) => {
		const classes = [styleFile?.[className] ?? className];

		if (additionalClasses) {
			for (const additionalClass of additionalClasses) {
				if (additionalClass) {
					classes.push(styleFile?.[additionalClass] ?? additionalClass);
				}
			}
		}

		return {
			className: classes.join(' '),
		};
	};

	return useClass;
};

export default useStyle;
