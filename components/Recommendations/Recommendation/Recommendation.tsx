import React, { ReactElement } from 'react';

import Image from 'next/image';

import useAnimation from 'components/Animation/useAnimation';
import useStyle from 'components/Layout/useStyle';
import { Recommendation as RecommendationType } from '../types';

import styles from './Recommendation.module.scss';

type Props = {
	index: number;
	recommendation: RecommendationType;
};

const Recommendation = ({ recommendation, index }: Props): ReactElement => {
	const style = useStyle(styles);
	const isOdd = index % 2;
	const { className: animation, style: animationStyle } = useAnimation(
		isOdd ? 'fadeInLeft' : 'fadeInRight',
		isOdd ? 'fadeOutRight' : 'fadeOutLeft',
		index * 300
	);

	return (
		<div {...style('section', isOdd ? 'odd' : 'even', animation)} style={animationStyle}>
			<div {...style('container')}>
				<div {...style('item')}>
					<div {...style('who')}>
						<a
							href={`https://www.linkedin.com/in/${recommendation.linkedIn}`}
							target="_blank"
							{...style('face')}
							rel="noreferrer"
						>
							<Image src={recommendation.face} alt={recommendation.name} />
						</a>
						<div {...style('about')}>
							<a
								href={`https://www.linkedin.com/in/${recommendation.linkedIn}`}
								target="_blank"
								{...style('name')}
								rel="noreferrer"
							>
								{recommendation.name}
							</a>
							<div {...style('function')}>{recommendation.inFunction}</div>
							<div {...style('years')}>
								worked together for {recommendation.yearsWorkedTogether} years
							</div>
						</div>
					</div>
					<div {...style('what')}>
						<div {...style('comment')}>{recommendation.comment}</div>
						<div {...style('date')}>{recommendation.date}</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Recommendation;
