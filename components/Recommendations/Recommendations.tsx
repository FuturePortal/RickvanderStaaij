import React, { ReactElement } from 'react';

import recommendations from './data';
import Recommendation from './Recommendation';

const Recommendations = (): ReactElement => (
	<>
		{recommendations.map((recommendation, index) => (
			<Recommendation key={index} index={index} recommendation={recommendation} />
		))}
	</>
);

export default Recommendations;
