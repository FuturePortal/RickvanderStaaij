import { Recommendation } from './types';

import FaceHermen from './face/hermen.png';
import FaceNickB from './face/nick.png';
import FaceNickS from './face/nick2.png';
import FacePim from './face/pim.png';
import FaceRalphine from './face/ralphine.png';
import FaceStefan from './face/stefan.png';
import FaceVincent from './face/vincent.png';
import FaceWolf from './face/wolf.png';

const recommendations: Recommendation[] = [
	{
		key: 'stefan',
		name: 'Stefan',
		inFunction: 'Senior DevOps engineer',
		date: '2023-01-23',
		yearsWorkedTogether: 8,
		comment:
			'Rick is a man with passion and commitment for his work. If he goes for it, he goes for it 200%. ' +
			'Communication wise he is very strong and transparent, and he is a great team player. ' +
			'I am lucky to have had Rick as a direct colleague at Enrise. ' +
			'To anyone who has the opportunity to hire Rick or work with him: do not think twice, do it.',
		linkedIn: 'svanessen',
		face: FaceStefan,
	},
	{
		key: 'hermen',
		name: 'Hermen',
		inFunction: 'Entreprenerd / Owner of Enrise',
		date: '2022-06-06',
		yearsWorkedTogether: 8,
		comment:
			'Rick is gedreven en professionele developer, die naast code kloppen ook in staat is een positieve ' +
			'inbreng te hebben op meerdere vlakken in de bedrijfsvoering. We hebben veel jaren samengewerkt met ' +
			'grote tevredenheid!',
		linkedIn: 'heinen',
		face: FaceHermen,
	},
	{
		key: 'nick2',
		name: 'Nick',
		inFunction: 'Software engineer',
		date: '2022-03-21',
		yearsWorkedTogether: 2,
		comment:
			'Naast de meer dan senioren kennis die Rick heeft opgebouwd in de afgelopen 8+ jaar bij Enrise is ' +
			'hij een hele fijne collega om mee samen te werken en in je omgeving te hebben, binnen én buiten ' +
			'kantoor. Rick weet als de beste hoe je op een gestructureerde en zorgvuldige manier werkt in een team ' +
			'en wijkt hier ook niet van af, tenzij het nog beter kan! De manier van kennisoverdracht is zeer prettig ' +
			'en heeft mij in een relatief korte tijd onwijs veel bijgeleerd. Rick gaan we nog voor een langere tijd ' +
			'missen in ons team.',
		linkedIn: 'nickspaargaren',
		face: FaceNickS,
	},
	{
		key: 'ralphine',
		name: 'Ralphine',
		inFunction: 'Software engineer',
		date: '2022-03-20',
		yearsWorkedTogether: 2,
		comment:
			'Rick is gewoonweg een top-collega. Hij is heel goed in het uitleggen van complexe begrippen en code. ' +
			"Daarnaast kan hij eerlijk en duidelijk feedback geven op bedrijf en collega's. Hij doet wat met de " +
			'ervaring die hij heeft en deelt daarvan uit aan de mensen om hem heen.',
		linkedIn: 'ralphine-van-braak-856755123',
		face: FaceRalphine,
	},
	{
		key: 'pim',
		name: 'Pim',
		inFunction: 'Business developer & partnerships',
		date: '2022-03-18',
		yearsWorkedTogether: 5,
		comment:
			'Ik heb 5 jaar met Rick samengewerkt bij het beste internetbureau van Nederland en in mijn commerciële ' +
			'rol heb vaak met Rick samengewerkt. Wij hebben regelmatig introducties bij organisaties gedaan waarbij ' +
			'Rick gepassioneerd kon vertellen over de beste online technologieën, Agile projectaanpak, DevOps ' +
			'integratie en uiteraard hoe je een belachelijk snelle frontends kunt bouwen. Rick is een fijne ' +
			'teamplayer, doordacht en helder in zijn communicatie en zet zijn kennis goed in om gezamenlijk tot ' +
			'de beste (goed onderbouwde) beslissing te komen.',
		linkedIn: 'pimvanderlinden',
		face: FacePim,
	},
	{
		key: 'vincent',
		name: 'Vincent',
		inFunction: 'Senior software engineer',
		date: '2022-03-16',
		yearsWorkedTogether: 2,
		comment:
			'Absolute topper! Een kritische denker, super ijverig, pakt snel en adequate dingen op. ' +
			'En staat zijn mannetje in een (inhoudelijke) discussie.',
		linkedIn: 'vinnie-hagen-',
		face: FaceVincent,
	},
	{
		key: 'wolf',
		name: 'Wolf',
		inFunction: 'Product owner',
		date: '2022-03-17',
		yearsWorkedTogether: 2,
		comment:
			'Ik kan Rick van harte aanbevelen! Ik heb Rick ervaren als een serieus harde werker met een kritische ' +
			'blik op development en organisatie. Rick weet vaak de vinger op de zere plek te leggen en staat zelf ' +
			'ook altijd open voor feedback. Naast dat alles is Rick ook nog een kei in organiseren en ' +
			'procesoptimalisatie. Al met al: als je de kans hebt - neem hem aan!',
		linkedIn: 'wolfhijlkema',
		face: FaceWolf,
	},
	{
		key: 'nick',
		name: 'Nick',
		inFunction: 'Senior software engineer',
		date: '2015-12-10',
		yearsWorkedTogether: 3,
		comment:
			'I have worked with Rick van der Staaij on a big Telecom greenfield project. ' +
			'Because we started with a clean slate he had a huge impact on the tools and solutions we used. ' +
			'He pushed for quality assurance tools including linters and kept a very close watch on the Front end ' +
			'PHP and JS code. He is very driven and takes and gives constructive feedback to peers and management. ' +
			'He delivers and tries to make sure everyone else also delivers by pushing for team work and ' +
			'constructivism. It was the first time I worked with him and after more than 10 months on the project ' +
			'I was still very happy he was on my team and would have him on my team again in a heart beat.',
		linkedIn: 'nickbelhomme',
		face: FaceNickB,
	},
];

export default recommendations;
