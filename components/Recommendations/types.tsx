import { StaticImageData } from 'next/image';

export type Recommendation = {
	key: string;
	name: string;
	yearsWorkedTogether: number;
	inFunction: string;
	date: string;
	comment: string;
	linkedIn: string;
	face: StaticImageData;
};
