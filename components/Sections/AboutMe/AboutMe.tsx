import React, { ReactElement } from 'react';

import Image from 'next/image';

import useAnimation from 'components/Animation/useAnimation';
import useStyle from 'components/Layout/useStyle';

import RickKingOfTheHill from './king-of-the-hill.jpg';

import styles from './AboutMe.module.scss';

const AboutMe = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInDown', 'fadeOutUp');
	const style = useStyle(styles);

	return (
		<div {...style('section', '--gray', animation)} style={animationStyle}>
			<div {...style('container', '--with-columns')}>
				<div>
					<h1>Welcome!</h1>
					<p {...style('intro')}>
						Hey, awesome that you take a peek at my personal website! I&apos;m Rick van der Staaij.
						All-round software developer, amateur power lifter, car/motorcycle enthusiast and great at more
						computer stuff. Feel free to roam around.
					</p>
				</div>
				<div {...style('image-column')}>
					<Image
						{...style('image')}
						src={RickKingOfTheHill}
						alt="Rick van der Staaij, king of the hill"
						priority
					/>
				</div>
			</div>
		</div>
	);
};

export default AboutMe;
