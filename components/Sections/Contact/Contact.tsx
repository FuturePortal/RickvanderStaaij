import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import useStyle from 'components/Layout/useStyle';
import ContactForm from './ContactForm';
import Map from './Map';

import styles from './Contact.module.scss';

const Contact = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInRight', 'fadeOutRight');
	const style = useStyle(styles);

	return (
		<div {...style('layout')}>
			<Map />
			<div {...style('form', animation)} style={animationStyle}>
				<ContactForm />
			</div>
		</div>
	);
};

export default Contact;
