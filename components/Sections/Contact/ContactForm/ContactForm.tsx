import React, { ReactElement, useState } from 'react';
import Image from 'next/image';
import { FormProvider, useForm } from 'react-hook-form';

import useStyle from 'components/Layout/useStyle';
import Input from './Input';

import CatMailGif from './mail-received.gif';

import styles from './ContactForm.module.scss';

export type ContactFormValues = {
	name: string;
	subject: string;
	contact: string;
	message: string;
};

const ContactForm = (): ReactElement => {
	const [isSent, setSent] = useState(false);
	const [isSending, setSending] = useState(false);
	const [errorMessage, setErrorMessage] = useState('');
	const form = useForm<ContactFormValues>();
	const style = useStyle(styles);

	const onSubmit = async (data): Promise<void> => {
		if (isSending) {
			return;
		}

		setSending(true);

		try {
			const response = await fetch('/api/send-mail', {
				method: 'POST',
				headers: {
					'content-type': 'application/json',
				},
				body: JSON.stringify(data),
			});

			if (response.ok) {
				setSent(true);
			} else {
				const result = await response.json();
				setErrorMessage(`Your message was not sent. ${result.message}`);
			}
		} catch (_error) {
			setErrorMessage('Unknown error.');
		} finally {
			setSending(false);
		}
	};

	if (isSent) {
		return (
			<>
				<h3>Thank you!</h3>
				<p>Thank you for your message! I will get back to you as soon as possible.</p>
				<p>
					<Image {...style('image')} src={CatMailGif} alt="cat received your email" />
				</p>
			</>
		);
	}

	return (
		<FormProvider {...form}>
			<form onSubmit={form.handleSubmit(onSubmit)}>
				<Input
					label="What's your name?"
					name="name"
					options={{
						required: 'Your name is required',
						minLength: {
							value: 3,
							message: 'Your name please',
						},
						pattern: {
							value: /^[^0-9]{1,}$/,
							message: 'This is not a name',
						},
					}}
				/>
				<Input
					label="What is it about?"
					name="subject"
					options={{
						required: 'What is it about?',
						minLength: {
							value: 3,
							message: 'What is it about?',
						},
					}}
				/>
				<Input
					label="How can I contact you?"
					help="Phone number or email address"
					name="contact"
					options={{
						required: 'How can I contact you?',
						minLength: {
							value: 6,
							message: 'How can I contact you?',
						},
					}}
				/>
				<Input
					label="Message"
					name="message"
					type="textarea"
					options={{
						required: 'Did you really forget a message?',
						minLength: {
							value: 20,
							message: 'You call this a message?',
						},
					}}
				/>
				{errorMessage && <div {...style('error')}>{errorMessage}</div>}
				<button {...style('submit')} type="submit">
					{isSending ? 'Sending...' : 'Send message'}
				</button>
			</form>
		</FormProvider>
	);
};

export default ContactForm;
