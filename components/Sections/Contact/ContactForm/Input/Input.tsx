import React, { ReactElement } from 'react';

import { RegisterOptions, useFormContext } from 'react-hook-form';

import useStyle from 'components/Layout/useStyle';

import styles from './Input.module.scss';

type Props = {
	name: string;
	label: string;
	help?: string;
	type?: 'text' | 'textarea';
	options?: RegisterOptions;
};

const Input = ({ name, label, type = 'text', help, options }: Props): ReactElement => {
	const {
		register,
		formState: { errors },
	} = useFormContext();
	const style = useStyle(styles);

	const getInput = (): ReactElement => {
		switch (type) {
			default:
				return <input {...style('text')} {...register(name, options)} />;
			case 'textarea':
				return <textarea {...style('textbox')} rows={8} {...register(name, options)} />;
		}
	};

	return (
		<label {...style('label')}>
			{label}
			{help && <span>{help}</span>}
			{getInput()}
			{errors[name] && <div {...style('error')}>{String(errors[name].message)}</div>}
		</label>
	);
};

export default Input;
