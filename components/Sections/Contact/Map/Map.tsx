import React, { ReactElement } from 'react';

import Image from 'next/image';

import useAnimation from 'components/Animation';
import useStyle from 'components/Layout/useStyle';

import mapImage from './map.png';

import styles from './Map.module.scss';

const ContactMap = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeIn', 'fadeOut');
	const style = useStyle(styles);

	return (
		<Image
			{...style('map', animation)}
			style={animationStyle}
			src={mapImage}
			alt="map pointing to Amersfoort"
			priority
		/>
	);
};

export default ContactMap;
