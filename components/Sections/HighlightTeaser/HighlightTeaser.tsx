import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation/useAnimation';
import BackAtEnrise from 'components/Highlights/BackAtEnrise';
import BotScripter from 'components/Highlights/BotScripter';
import HighlightList from 'components/Highlights/List';
import Nawcast from 'components/Highlights/Nawcast';
import Heading from 'components/Layout/Heading';
import useStyle from 'components/Layout/useStyle';

const HighlightTeaser = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeIn', 'fadeOut', 300);
	const style = useStyle();

	return (
		<div {...style('section', animation)} style={animationStyle}>
			<div {...style('container')}>
				<Heading title="Highlights" url="/highlights" />
				<HighlightList highlights={[BotScripter, Nawcast, BackAtEnrise]} />
			</div>
		</div>
	);
};

export default HighlightTeaser;
