import React, { ReactElement } from 'react';

import Image from 'next/image';

import useAnimation from 'components/Animation/useAnimation';
import Heading from 'components/Layout/Heading';
import useStyle from 'components/Layout/useStyle';

import Amersfoort from './images/amersfoort.jpg';
import DeadliftPlateau from './images/deadlift-plateau.jpg';
import CarImage from './images/ford-focus-titanium-2015.jpg';
import Merle from './images/merle.jpg';
import MouwImage from './images/mouw-2015.jpg';
import MotorImage from './images/yamaha-r1-2011.jpg';

import styles from './Hobbies.module.scss';

const HobbyImages = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInUp', 'fadeOutDown', 1200);
	const style = useStyle(styles);

	return (
		<div {...style('section', animation)} style={animationStyle}>
			<div {...style('container')}>
				<Heading title="What makes me tick" />
				<Image {...style('image')} src={Amersfoort} alt="Amersfoort koppelpoort" />
				<Image {...style('image')} src={Merle} alt="Merle Kroesen" />
				<Image {...style('image')} src={DeadliftPlateau} alt="Deadlift plateau" />
				<Image {...style('image')} src={MotorImage} alt="Yamaha R1 2011" />
				<Image {...style('image')} src={CarImage} alt="Ford Focus Titanium 2015" />
				<Image {...style('image')} src={MouwImage} alt="Mouw, a kitty born in 2015" />
			</div>
		</div>
	);
};

export default HobbyImages;
