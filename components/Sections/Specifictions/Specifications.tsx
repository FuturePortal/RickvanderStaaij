import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import useStyle from 'components/Layout/useStyle';
import cards, { SiteSpecification } from './specifications';

import styles from './Specifications.module.scss';

type Props = {
	specification: SiteSpecification;
	index: number;
};

const SpecificationCard = ({ specification, index }: Props) => {
	const { className: animation, style: animationStyle } = useAnimation(
		'fadeInRight',
		'fadeOutLeft',
		300 + index * 300
	);
	const style = useStyle(styles);

	return (
		<div {...style('card', animation)} style={animationStyle}>
			<h2>{specification.title}</h2>
			<p>{specification.content}</p>
			{specification.link && (
				<p>
					<a href={specification.link} rel="noreferrer" target="_blank">
						Find out more
					</a>
				</p>
			)}
		</div>
	);
};

const Specifications = (): ReactElement => {
	const animation = useAnimation();
	const style = useStyle(styles);

	return (
		<div className="section">
			<div className="container">
				<div {...animation}>
					<h1>What makes this website awesome for developers?</h1>
					<p>
						Building a website can be super easy, or extremely complicated. Using the right tools and
						techniques allows us to create a website with technical excellence, but also with great speed.
						Below is a list of tools and techniques used for building this website.
					</p>
				</div>
				<div {...style('cards')}>
					{cards.map((specification, index) => (
						<SpecificationCard key={index} index={index} specification={specification} />
					))}
				</div>
			</div>
		</div>
	);
};

export default Specifications;
