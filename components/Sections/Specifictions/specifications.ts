export type SiteSpecification = {
	title: string;
	content: string;
	link?: string;
};

const specifications: SiteSpecification[] = [
	{
		title: 'Open source',
		content: 'I love open source. So why not make this website open source?',
		link: 'https://gitlab.com/FuturePortal/RickvanderStaaij',
	},
	{
		title: 'NextJS (server side rendered + SPA)',
		content: `NextJS is awesome in that it's always rendered server side, and then continues as single page
            application. So great for robots and humans!`,
		link: 'https://nextjs.org/',
	},
	{
		title: 'Kubernetes on DigitalOcean',
		content: `Hosted on the trusted and stable DigitalOcean Kubernetes service. Running a kubernetes cluster,
            allowing high-availability and scaling.`,
		link: 'https://www.digitalocean.com/products/kubernetes/',
	},
	{
		title: 'Review applications',
		content: `For every new feature I create, a review application is set up in the cloud.
            Allowing me to share new features with other people before they are actually on production.`,
	},
	{
		title: 'Continuous Deployment',
		content: `The build process is completely automated. All I have to do is accept new code and
            it will be automatically tested and deployed to production.`,
	},
	{
		title: `Let's Encrypt`,
		content: 'Fully https encrypted with this awesome service, manged by the kubernetes cluster.',
		link: 'https://letsencrypt.org/',
	},
	{
		title: 'GitLab (CI)',
		content: `The code is hosted on GitLab, a service that I love. The build process is fully
            automated via GitLab-CI.`,
		link: 'https://docs.gitlab.com/ee/ci/',
	},
	{
		title: 'Docker',
		content: 'Running in a docker container for quickly setting up and portability.',
		link: 'https://www.docker.com/',
	},
	{
		title: 'Responsive',
		content: 'Of course this website is fully responsive, looking great on any device!',
	},
	{
		title: 'Typescript',
		content: 'Build using Typescript, making the code beautiful and easier to understand.',
		link: 'https://nextjs.org/',
	},
	{
		title: 'SEO optimized',
		content: `Perfectly indexed by Google. Providing a nice sitemap.xml, robots.txt, and 404 status
            codes for pages that don't exist.`,
	},
];

export default specifications;
