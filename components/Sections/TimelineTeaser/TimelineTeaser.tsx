import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation/useAnimation';
import useInternalLink from 'components/Layout/Go/useInternalLink';
import useStyle from 'components/Layout/useStyle';
import Badge from 'components/Timeline/Badge';

import styles from './TimelineTeaser.module.scss';

const TimelineTeaser = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInUp', 'fadeOutDown', 800);
	const href = useInternalLink('/career');
	const style = useStyle(styles);

	return (
		<div {...style('section', animation)} style={animationStyle}>
			<div {...style('container')}>
				<a {...style('teaser')} {...href}>
					<div {...style('bubble')}>I joined the earth</div>
					<div {...style('timeline')}>
						<Badge year="1991" icon="celebration" />
						<Badge year="2013" icon="internship" />
						<Badge year="2014" icon="diploma" />
						<Badge year="2016" icon="blog" />
						<Badge year={`${new Date().getFullYear()}`} icon="job" />
					</div>
					<div {...style('bubble', 'bubble--last')}>
						Senior full stack developer at <span>Enrise</span>
					</div>
				</a>
			</div>
		</div>
	);
};

export default TimelineTeaser;
