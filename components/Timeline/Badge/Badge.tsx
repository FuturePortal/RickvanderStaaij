import React, { ReactElement } from 'react';

import useStyle from 'components/Layout/useStyle';

import BlogSvg from './icon/blog.svg';
import DiplomaSvg from './icon/diploma.svg';
import InternshipSvg from './icon/internship.svg';
import JobSvg from './icon/job.svg';
import CelebrationSvg from './icon/party.svg';
import VideoSvg from './icon/video.svg';

import styles from './Badge.module.scss';

const icons = {
	video: VideoSvg,
	job: JobSvg,
	internship: InternshipSvg,
	diploma: DiplomaSvg,
	blog: BlogSvg,
	celebration: CelebrationSvg,
};

type Props = {
	year: string;
	icon: 'video' | 'job' | 'internship' | 'diploma' | 'blog' | 'celebration';
	float?: boolean;
};

const Badge = ({ year, icon, float = false }: Props): ReactElement => {
	const style = useStyle(styles);
	const IconSvg = icons[icon] || JobSvg;

	return (
		<div {...style('badge', float ? 'float' : null)}>
			<IconSvg />
			<div {...style('year')}>{year}</div>
		</div>
	);
};

export default Badge;
