import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<>
		<p>
			Finally a study that interested me, &apos;MBO application development (level 4)&apos;. As I did what I do
			best, I managed to complete the study in two and a half year, instead of the regular four years.
		</p>
		<p>
			After this study, I directly continued in the second half year of &apos;HBO Informatica&apos;, a follow-up
			bachelor study.
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Application developer',
	date: moment('2010-02-08'),
	content: <EventContent />,
	type: 'diploma',
};

export default event;
