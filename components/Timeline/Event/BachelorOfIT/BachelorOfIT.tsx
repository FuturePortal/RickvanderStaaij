import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<>
		<p>I finished my course &apos;HBO Informatica&apos; on the University of Utrecht. I am now a bachelor of IT.</p>
		<ul>
			<li>Specialisation: Advanced software engineering</li>
			<li>Minor: Web & Multimedia</li>
		</ul>
		<p>
			I passed with merit (meaning my average and final grade was an 8 out of 10). I finished the course in 3
			years instead of the regular 4 years.
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Bachelor of IT',
	date: moment('2014-01-20'),
	content: <EventContent />,
	type: 'diploma',
};

export default event;
