import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<>
		<p>
			January 2016 was the second time I attended PHPBenelux, and it once again became more obvious how the
			community is a huge part of the flexible language, PHP. Next to the newest and coolest tools, tricks and
			frameworks, personal development was a huge topic on the conferences I’ve attended. In the blog post I’ve
			written about the leanings I took from those sessions and what information I gathered over the years.
		</p>
		<p>
			<a href="https://enrise.com/2016/04/how-to-become-a-better-developer/" target="_blank" rel="noreferrer">
				Read it on the Enrise TechBlog
			</a>
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Blog: How to become a better developer',
	date: moment('2016-04-12'),
	content: <EventContent />,
	type: 'blog',
};

export default event;
