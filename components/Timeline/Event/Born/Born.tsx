import moment from 'moment';

const event: TimelineEvent = {
	title: 'I was born',
	date: moment('1991-11-08'),
	type: 'celebration',
};

export default event;
