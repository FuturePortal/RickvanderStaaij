import React, { ReactElement } from 'react';

import moment from 'moment';

import Video from 'components/Video';

import PreviewImage from './video-preview.png';

const EventContent = (): ReactElement => (
	<>
		<p>
			For CodeCuisine®Live, an event from Enrise, I gave a (Dutch) presentation about working with Continuous
			Deployment and Review Applications together with Stefan van Essen. It will show how an enterprise
			application can be brought to production a lot quicker, without giving in on testability.
		</p>
		<Video
			title="Continuous Deployment with Review Applications"
			youtubeKey="tJQF_Lk3ixI"
			previewImage={PreviewImage}
		/>
	</>
);

const event: TimelineEvent = {
	title: 'Continuous Deployment with Review Applications',
	date: moment('2017-10-03'),
	content: <EventContent />,
	type: 'video',
};

export default event;
