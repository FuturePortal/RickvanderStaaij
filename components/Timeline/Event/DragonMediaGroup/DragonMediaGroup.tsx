import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<p>
		I started working at DragonMediaGroup as an internship. After a half year of working on my web development
		skills I kept working at Dragon Media Group next to my study and in vacations.
	</p>
);

const event: TimelineEvent = {
	title: 'Developer at Dragon Media Group',
	date: moment('2009-06-05'),
	till: moment('2011-02-14'),
	content: <EventContent />,
	type: 'internship',
};

export default event;
