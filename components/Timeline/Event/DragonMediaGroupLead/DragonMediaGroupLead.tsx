import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<p>
		Being the go-to-guy in the company when it came to development questions, my job title changed to &quot;lead
		developer&quot;. With this title I&apos;ve taken more responsibility in the projects that were running, and I
		worked on the development process together with setting up a centralised content management system.
	</p>
);

const event: TimelineEvent = {
	title: 'Lead developer at Dragon Media Group',
	date: moment('2011-02-01'),
	till: moment('2012-06-01'),
	content: <EventContent />,
	type: 'job',
};

export default event;
