import React, { ReactElement } from 'react';

import moment from 'moment';
import Image from 'next/image';

import EagerlyImage from './eagerly-transfer.png';

const EventContent = (): ReactElement => (
	<>
		<p>
			For my education &apos;HBO Informatica&apos; I&apos;ve had to do a graduation internship. For my paper and
			internship I&apos;ve worked on Eagerly&apos;s narrow-casting system. I did research on what planning tool
			should be most efficient to use and I basically rebuild their narrow-casting product. In Eagerly I will be
			remembered as &apos;RedBull Rick&apos;.
		</p>
		<p>
			I&apos;ve had a successful internship and I graduated with an 8 (out of 10) as grade for the internship and
			paper.
		</p>
		<p>
			<Image src={EagerlyImage} alt="Me working at Enrise" />
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Final internship at Eagerly',
	date: moment('2013-09-1'),
	till: moment('2014-02-14'),
	content: <EventContent />,
	type: 'internship',
};

export default event;
