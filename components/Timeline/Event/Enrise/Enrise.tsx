import React, { ReactElement } from 'react';

import moment from 'moment';
import Image from 'next/image';

import FirstDayImage from './me-at-enrise-scaled.png';

const EventContent = (): ReactElement => (
	<>
		<p>
			Full-time all-round web development. Working with continuous integration in a truly Agile/Scrum environment.
			Creating websites, API&apos;s, web- and mobile applications. More details can be found at{' '}
			<a href="https://enrise.com" rel="noreferrer" target="_blank">
				enrise.com
			</a>
			.
		</p>
		<p>
			<Image src={FirstDayImage} alt="Me working at Enrise" />
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Allround developer at Enrise',
	date: moment('2014-02-10'),
	till: moment(),
	content: <EventContent />,
	type: 'job',
};

export default event;
