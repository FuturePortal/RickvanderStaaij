import React, { ReactElement } from 'react';

import moment from 'moment';
import Image from 'next/image';

import BackAtEnrise from '../../../Highlights/BackAtEnrise';
import useInternalLink from '../../../Layout/Go/useInternalLink';

import SignEnriseContract from './sign-contract.jpg';

const EventContent = (): ReactElement => {
	const blogHref = useInternalLink(`/highlight/${BackAtEnrise.slug}`);

	return (
		<>
			<p>
				Full-time all-round web development. Working with continuous deployment in a truly Agile/Scrum
				environment. Creating websites, API&apos;s, web- and mobile applications. More details can be found at{' '}
				<a href="https://enrise.com" rel="noreferrer" target="_blank">
					enrise.com
				</a>
				.
			</p>
			<p>
				<Image src={SignEnriseContract} alt="Signing back to Enrise" />
			</p>
			<p>
				I&apos;m back! <a {...blogHref}>Read more</a> about why I&apos;ve returned.
			</p>
		</>
	);
};

const event: TimelineEvent = {
	title: 'Senior full stack developer at Enrise',
	date: moment('2023-09-01'),
	content: <EventContent />,
	type: 'job',
};

export default event;
