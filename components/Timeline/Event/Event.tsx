import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import useStyle from 'components/Layout/useStyle';
import Badge from '../Badge';

import styles from './Event.module.scss';

type Props = {
	event: TimelineEvent;
	odd: boolean;
	index: number;
};

const Event = ({ event, odd, index }: Props): ReactElement => {
	const delay = 1000 + index * 300;
	const { className: animation, style: animationStyle } = useAnimation(
		odd ? 'fadeInLeft' : 'fadeInRight',
		odd ? 'fadeOutLeft' : 'fadeOutRight',
		delay
	);
	const style = useStyle(styles);

	return (
		<li {...style('item')}>
			<Badge year={event.date.format('YYYY')} icon={event.type} float />
			<div {...style('panel', odd ? 'panel--odd' : null, animation)} style={animationStyle}>
				<h2>{event.title}</h2>
				<p>
					{event.date.format('D MMMM YYYY')} {event.till && `till ${event.till.format('D MMMM YYYY')}`}
				</p>
				{event.content}
			</div>
		</li>
	);
};

export default Event;
