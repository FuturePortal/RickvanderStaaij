import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<p>
		In the first year of my MBO study, I had to go one day of the week to a place where I could practice IT. I found
		a primary school near to my house where I went every thursday to mostly manage the schools computers and its
		network. A lot of people were greatful for my work at the primary school.
	</p>
);

const event: TimelineEvent = {
	title: 'First internship in the IT',
	date: moment('2009-01-05'),
	till: moment('2009-06-14'),
	content: <EventContent />,
	type: 'internship',
};

export default event;
