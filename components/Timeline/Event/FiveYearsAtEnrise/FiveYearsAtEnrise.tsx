import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<>
		<p>
			I&apos;m working at Enrise for 5 years now. It might seem like a long time, but I&apos;ve learned, and still
			am learning a lot! When I joined 5 years ago I was a Junior developer. I did have quite some experience but
			Enrise opened my eyes to the open source world. Instead of building everything on my own, I dove in the
			world of composing high quality enterprise applications.
		</p>
		<p>
			Nowadays I can be considered a senior developer. Front-end, back-end, DevOps. Basically going from idea to
			production. But that wasn&apos;t possible without all the smart people around me at Enrise. Thanks!
		</p>
	</>
);

const event: TimelineEvent = {
	title: '5 years at Enrise',
	date: moment('2019-02-10'),
	content: <EventContent />,
	type: 'job',
};

export default event;
