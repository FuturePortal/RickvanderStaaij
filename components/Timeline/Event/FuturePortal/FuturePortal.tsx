import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<p>
		Since June 2012 I registered my own company. I work here on some small projects beside my study and job, to
		spend my free time as efficient als possible! At this moment I&apos;m working on some personal projects, not
		really doing any external work anymore.
	</p>
);

const event: TimelineEvent = {
	title: 'Owner of FuturePortal',
	date: moment('2012-06-01'),
	content: <EventContent />,
	type: 'job',
};

export default event;
