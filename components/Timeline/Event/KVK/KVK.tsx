import React, { ReactElement } from 'react';

import moment from 'moment';
import Image from 'next/image';

import RickAtKVK from './kvk-wall.jpg';

const EventContent = (): ReactElement => (
	<>
		<p>
			I decided it is time to check out what else there could be for me. I found an interesting career
			opptertunity at KVK (the chamber of commerce). Even though it&apos;s a very different organisation, I
			decided to give it a shot as senior frontend developer.
		</p>
		<p>
			I&apos;ve pushed for code quality improvements, helped the team to improve their skills and finished a nice
			product in the new sales funnel.
		</p>
		<p>
			Unfortunately, due to a lack of pace and change I felt understimulated and desiring a more open and dynamic
			environment. So after only a couple of months I started searching for something else.
		</p>
		<p>
			<Image src={RickAtKVK} alt="Me working at Enrise" />
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Senior frontend developer at KVK',
	date: moment('2022-04-04'),
	till: moment('2022-08-31'),
	content: <EventContent />,
	type: 'job',
};

export default event;
