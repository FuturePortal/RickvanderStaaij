import React, { ReactElement } from 'react';

import moment from 'moment';
import Image from 'next/image';

import LabDigitalImage from './rick-at-lab-digital.jpg';

const EventContent = (): ReactElement => (
	<>
		<p>
			Lab Digital is a project company that works on enterprise commerce platforms. Using a MACH technology stack
			they quickly setup new environments with a world wide premium shopping experience.
		</p>
		<p>
			Personally, I was really missing the self-steering characteristics. Working under a manager, and lacking the
			agility that I desire to have full creative freedom in my work, I decided not to extend my contract.
		</p>
		<p>
			<Image src={LabDigitalImage} alt="Rick working at Lab Digital" />
		</p>
	</>
);

const event: TimelineEvent = {
	title: 'Senior javascript engineer at Lab Digital',
	date: moment('2022-09-01'),
	till: moment('2023-08-31'),
	content: <EventContent />,
	type: 'job',
};

export default event;
