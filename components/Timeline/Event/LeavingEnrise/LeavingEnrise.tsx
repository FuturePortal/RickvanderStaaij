import React, { ReactElement } from 'react';

import moment from 'moment';
import Image from 'next/image';

import LeavingEnrisePreview from './leaving-enrise.jpg';

const EventContent = (): ReactElement => (
	<>
		<p>
			After 8 great years at Enrise, I&apos;m moving along to a new opportunity. I&apos;ve grown and learned more
			than I could ever imagine. I couldn&apos;t leave Enrise empty handed, so I&apos;ve left them with a little
			present. I&apos;ll truly miss you{' '}
			<a href="https://enrise.com" target="_blank" rel="noreferrer">
				Enrise
			</a>
			!
		</p>
		<Image src={LeavingEnrisePreview} alt="Leaving Enrise" />
	</>
);

const event: TimelineEvent = {
	title: 'Leaving Enrise',
	date: moment('2022-03-18'),
	content: <EventContent />,
	type: 'job',
};

export default event;
