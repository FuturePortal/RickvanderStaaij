import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<p>
		Every day (except for sunday) I walked a paper round for about an hour. I did this next to my high school and I
		did it the first few months of my MBO study.
	</p>
);

const event: TimelineEvent = {
	title: 'Paper round',
	date: moment('2007-06-28'),
	till: moment('2009-05-16'),
	content: <EventContent />,
	type: 'job',
};

export default event;
