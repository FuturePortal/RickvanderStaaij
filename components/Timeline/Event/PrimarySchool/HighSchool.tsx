import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<>
		<p>
			Junior general secondary education, aka high-school VMBO&apos;t (now called MAVO again). With my lack of
			interest for the default courses, I started to learn programming. I easily passed my exams. Now I was
			finally able to start a study that I was actually interested in.
		</p>
		<p>After high-school I continued with a MBO IT study.</p>
	</>
);

const event: TimelineEvent = {
	title: 'Graduated from high-school',
	date: moment('2004-07-10'),
	till: moment('2008-06-12'),
	content: <EventContent />,
	type: 'diploma',
};

export default event;
