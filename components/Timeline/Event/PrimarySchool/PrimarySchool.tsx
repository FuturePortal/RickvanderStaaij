import React, { ReactElement } from 'react';

import moment from 'moment';

const EventContent = (): ReactElement => (
	<p>
		In primary school I was very creative. No big plans for the future yet. The final primary school test (CITO)
		advised me to go to the Senior general secondary education/pre-university education.
	</p>
);

const event: TimelineEvent = {
	title: 'Finished primary school',
	date: moment('1996-08-01'),
	till: moment('2004-07-01'),
	content: <EventContent />,
	type: 'diploma',
};

export default event;
