import React, { ReactElement } from 'react';

import moment from 'moment';

import Video from 'components/Video';

import WorkAtEnriseVideoPreview from './video-preview.png';

const EventContent = (): ReactElement => (
	<>
		<p>
			Since working at Enrise is still a great passion, and especially with the direction to become a fully self
			steering company, I&apos;ve participated in a new Enrise video.
		</p>
		<Video title="Working at Enrise video" youtubeKey="Ovb_deGdwcs" previewImage={WorkAtEnriseVideoPreview} />
	</>
);

const event: TimelineEvent = {
	title: 'Working at Enrise video',
	date: moment('2020-02-26'),
	content: <EventContent />,
	type: 'video',
};

export default event;
