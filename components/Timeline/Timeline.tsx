import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import useStyle from 'components/Layout/useStyle';
import Event from './Event';
import events from './events';

import styles from './Timeline.module.scss';

const Timeline = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInDown', 'fadeOut');
	const style = useStyle(styles);

	return (
		<div {...style('container', animation)} style={animationStyle}>
			<ul {...style('line')}>
				{events.map((event, index) => (
					<Event key={index} index={index} odd={!!(index % 2)} event={event} />
				))}
			</ul>
		</div>
	);
};

export default Timeline;
