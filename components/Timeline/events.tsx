import sortByDate from 'helpers/SortByDate';
import ApplicationDeveloper from './Event/ApplicationDeveloper';
import BachelorOfIT from './Event/BachelorOfIT';
import BetterDeveloperBlog from './Event/BetterDeveloperBlog';
import Born from './Event/Born';
import CDPresentation from './Event/CDPresentation';
import DragonMediaGroup from './Event/DragonMediaGroup';
import DragonMediaGroupLead from './Event/DragonMediaGroupLead';
import Eagerly from './Event/Eagerly';
import Enrise from './Event/Enrise';
import EnriseRejoin from './Event/EnriseRejoin';
import FirstITInternship from './Event/FirstITInternship';
import FiveYearsAtEnrise from './Event/FiveYearsAtEnrise';
import FuturePortal from './Event/FuturePortal';
import HighSchool from './Event/HighSchool';
import KVK from './Event/KVK';
import LabDigital from './Event/LabDigital';
import LeavingEnrise from './Event/LeavingEnrise';
import PaperRound from './Event/PaperRound';
import PrimarySchool from './Event/PrimarySchool';
import WorkAtEnriseVideo from './Event/WorkAtEnriseVideo';

const events: TimelineEvent[] = [
	Born,
	PrimarySchool,
	WorkAtEnriseVideo,
	FiveYearsAtEnrise,
	CDPresentation,
	BetterDeveloperBlog,
	Enrise,
	EnriseRejoin,
	BachelorOfIT,
	Eagerly,
	FuturePortal,
	DragonMediaGroupLead,
	ApplicationDeveloper,
	DragonMediaGroup,
	FirstITInternship,
	HighSchool,
	PaperRound,
	LeavingEnrise,
	KVK,
	LabDigital,
].sort(sortByDate);

export default events;
