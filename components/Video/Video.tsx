import React, { ReactElement, useState } from 'react';

import Image, { StaticImageData } from 'next/image';

import useStyle from 'components/Layout/useStyle';

import PlayIcon from './icon.svg';

import styles from './Video.module.scss';

type Props = {
	title: string;
	previewImage?: StaticImageData;
	youtubeKey: string;
};

const Video = ({ previewImage, title, youtubeKey }: Props): ReactElement => {
	const [play, setPlay] = useState(false);
	const style = useStyle(styles);

	if (play) {
		return (
			<button type="button" {...style('wrapper')}>
				<iframe
					width="560"
					height="315"
					src={`https://www.youtube.com/embed/${youtubeKey}?autoplay=1`}
					allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
					allowFullScreen
				/>
			</button>
		);
	}

	return (
		<button type="button" {...style('wrapper')} onClick={() => setPlay(true)}>
			{previewImage && <Image src={previewImage} alt={title} />}
			<PlayIcon {...style('icon')} />
		</button>
	);
};

export default Video;
