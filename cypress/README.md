# Cypress

Cypress is used for regression testing.

## Cypress in CI

You can simply run `cypress` to start testing when this container is build with the ci stage.

## Move assets

A script `move-assets` is added, which will target all png and mp4 files in your cypress/ folder, and move them to an
`assets/` folder. This is done to create easy to browse artifacts.
