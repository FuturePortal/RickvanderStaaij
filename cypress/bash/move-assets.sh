#!/bin/bash

rm -rf assets/;
echo 'Moving resulting test files to a single assets output folder.';
mkdir assets/;
echo 'Moving video files...';
find ./cypress/ -name '*.mp4' -exec mv '{}' ./assets/ \;
echo 'Moving images...';
find ./cypress/ -name '*.png' -exec mv '{}' ./assets/ \;
echo 'Move completed.';
