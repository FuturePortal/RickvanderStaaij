import { defineConfig } from 'cypress';

export default defineConfig({
	defaultCommandTimeout: 20000,
	pageLoadTimeout: 60000,
	reporter: 'spec',
	env: {
		APP_URL: 'http://rick.local',
		MAIL_URL: 'http://mail.rick.local',
		MAIL_TARGET: 'local@rick.nu',
	},
	chromeWebSecurity: false,
	videoCompression: false,
	e2e: {
		supportFile: 'support/e2e.ts',
		specPattern: 'e2e/**/*.cy.ts',
	},
	screenshotsFolder: 'screenshots/',
	videosFolder: 'videos/',
});
