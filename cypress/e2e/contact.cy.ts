const secretFile = 'fixtures/secret.txt';
const subject = 'Secret code';
const generateUniqueCode = (): string => [...Array(25)].map(() => Math.random().toString(36)[2]).join('');

describe('Send a mail using the contact page', function () {
	it('creates a secret', function () {
		cy.writeFile(secretFile, generateUniqueCode());
	});

	it('opens the contact page', function () {
		cy.readFile(secretFile).then((secret) => {
			cy.visit(`${Cypress.env('APP_URL')}/contact`);

			cy.contains('Send message');

			cy.get('input[name="name"]').type('Cypress Bot');
			cy.get('input[name="subject"]').type(subject);
			cy.get('input[name="contact"]').type(`${Cypress.env('APP_URL')}/contact`);
			cy.get('textarea[name="message"]').type(`Secret message. The secret is: ${secret}. Bye.`);
			cy.screenshot('contact-form');
			cy.get('button[type="submit"]').click();

			cy.contains('Thank you!');
		});
	});

	it('grabs the email from maildev', function () {
		cy.readFile(secretFile).then((secret) => {
			cy.getLatestMailBy(`Contact: ${subject}`, Cypress.env('MAIL_TARGET')).then((mail) => {
				cy.wrap(mail.text).should('contain', secret);

				cy.writeFile('fixtures/emails/contact-with-secret.html', mail.html);
			});
		});
	});

	it('opens the sent contact email', function () {
		cy.readFile(secretFile).then((secret) => {
			cy.visit('fixtures/emails/contact-with-secret.html');

			cy.contains(secret);

			cy.screenshot('contact-mail');
		});
	});
});
