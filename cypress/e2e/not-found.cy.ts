describe('check the not found page', function () {
	it('opens a non existing page', function () {
		cy.visit(`${Cypress.env('APP_URL')}/this-page-does-not-exist`, {
			failOnStatusCode: false,
		});

		cy.contains('Yeah, no');

		cy.screenshot('not-found');
	});

	it('opens a non existing file', function () {
		cy.visit(`${Cypress.env('APP_URL')}/images/non-existing-file.png`, {
			failOnStatusCode: false,
		});

		cy.contains('Yeah, no');
	});

	it('verifies 404 status codes for not found items', function () {
		cy.request({
			url: `${Cypress.env('APP_URL')}/this-page-does-not-exist`,
			failOnStatusCode: false,
		}).then((response) => {
			expect(response.status).to.eq(404);
		});

		cy.request({
			url: `${Cypress.env('APP_URL')}/images/non-existing-file.png`,
			failOnStatusCode: false,
		}).then((response) => {
			expect(response.status).to.eq(404);
		});
	});
});
