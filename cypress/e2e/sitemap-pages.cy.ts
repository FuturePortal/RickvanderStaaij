describe('check the homepage', function () {
	it('opens the sitemap and checks its pages', function () {
		cy.request({
			method: 'get',
			url: `${Cypress.env('APP_URL')}/sitemap.xml`,
		}).then((response) => {
			const parser = new DOMParser();

			const parsedSitemap = parser.parseFromString(response.body, 'text/xml');

			for (let locationElements of Array.from(parsedSitemap.getElementsByTagName('loc'))) {
				const pageUrl = locationElements.getHTML();
				cy.log(`Checking ${pageUrl}`);

				// check if the resource exists
				cy.request('HEAD', pageUrl).its('status').should('eq', 200);

				// check if the resource exists AND download it
				cy.request(pageUrl).its('status').should('eq', 200);

				// visit the page to check if it loads in the browser
				cy.visit(pageUrl);
			}
		});
	});
});
