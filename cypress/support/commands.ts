// https://on.cypress.io/custom-commands

declare global {
	/* eslint-disable-next-line @typescript-eslint/no-namespace */
	namespace Cypress {
		interface Chainable {
			path: typeof path;
		}
	}
}

export const path = (uri: string): void => {
	cy.location().should((loc) => {
		expect(loc.pathname).to.eq(uri);
	});
};

Cypress.Commands.add('path', path);
