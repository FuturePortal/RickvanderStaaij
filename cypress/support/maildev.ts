declare global {
	/* eslint-disable-next-line @typescript-eslint/no-namespace */
	namespace Cypress {
		interface Chainable {
			getLatestMailBy: typeof getLatestMailBy;
		}
	}
}

export type MailAddress = {
	address: string;
	name: string;
};

export type Mail = {
	id: string;
	subject: string;
	html: string;
	text: string;
	time: string;
	from: MailAddress[];
	to: MailAddress[];
};

export type MailTarget = {
	subject: string;
};

const newestMailFirst = (mailA: Mail, mailB: Mail): number =>
	new Date(mailB.time).getTime() - new Date(mailA.time).getTime();

const getLatestMailBy = (partialSubject: string, receiverMailAddress: string): Cypress.Chainable<Mail> => {
	return cy
		.request({
			method: 'GET',
			url: `${Cypress.env('MAIL_URL')}/email`,
		})
		.then((response) => response.body as Mail[])
		.then((mails) => mails.sort(newestMailFirst))
		.then((mails) =>
			mails.find(
				(mail) =>
					mail.subject.indexOf(partialSubject) !== -1 &&
					mail.to.find((mailReceiver) => mailReceiver.address === receiverMailAddress)
			)
		);
};

Cypress.Commands.add('getLatestMailBy', getLatestMailBy);
