import { Moment } from 'moment';

const sortByDate = <T extends { date: Moment }>(itemA: T, itemB: T): number => itemB.date.unix() - itemA.date.unix();

export default sortByDate;
