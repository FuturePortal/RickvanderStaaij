# Kubernetes deployment

Only the project specific configuration is in this project. In order to get the websites live, more general
Kubernetes config is required. This is managed in a separate repository for my
[personal kubernetes cluster](https://gitlab.com/FuturePortal/PersonalCloudSetup).
