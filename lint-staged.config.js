module.exports = {
	'**/*.{js,ts,tsx}': ['eslint --fix', 'prettier --write'],

	'**/*.{ts,tsx}': [
		// tsc runs for all files instead of only the edited ones
		() => 'tsc --noEmit --pretty',
	],

	'**/*.{md,json}': 'prettier --write',

	'**/*.scss': ['stylelint --fix', 'prettier --write'],
};
