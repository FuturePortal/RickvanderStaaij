### 2 broodjes gezond (600 kcal)

aantal | van | kcal
--- | --- | ---
2 | bruine bol | 225
2 | plak kaas | 190
2 | plak kipfilet | 44
2 | ei | 128
2 | sla | 4

### Kip-wraps quick & easy (650 kcal)

aantal | van | kcal
--- | --- | ---
2 | volkoren wrap | 224
2 | plak kaas | 190
100g | gerookte kip | 118
5 plakken | kipfilet | 110
50g | sla | 10

### Kwark whey (410 kcal)

aantal | van | kcal
--- | --- | ---
250g | franse magere kwark | 150
35g | whey | 136
25g | chia/gebroken lijnzaad | 120

### Bakkie time (460 kcal)

aantal | van | kcal
--- | --- | ---
1 | bakkie kip groenten rijst | 460

### Gym time (45 kcal)

aantal | van | kcal
--- | --- | ---
12g | pre workout | 25
8g | creatine | 20
