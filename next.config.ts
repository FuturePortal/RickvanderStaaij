import path from 'path';

/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
	sassOptions: {
		includePaths: [path.join(__dirname, './style/')],
		prependData: `@import "foundation";`,
	},
	async redirects() {
		return [
			{
				source: '/cimonitor',
				destination: '/ci',
				permanent: true,
			},
			{
				source: '/creation',
				destination: '/highlightss',
				permanent: true,
			},
			{
				source: '/creations',
				destination: '/highlights',
				permanent: true,
			},
			{
				source: '/creation/:path*',
				destination: '/highlight/:path*',
				permanent: true,
			},
			{
				source: '/work',
				destination: '/highlights',
				permanent: true,
			},
			{
				source: '/work/:path*',
				destination: '/highlight/:path*',
				permanent: true,
			},
			{
				source: '/cv',
				destination: '/career',
				permanent: true,
			},
			{
				source: '/resume',
				destination: '/career',
				permanent: true,
			},
			{
				source: '/timeline',
				destination: '/career',
				permanent: true,
			},
			{
				source: '/awesomeness',
				destination: '/site-specifications',
				permanent: true,
			},
			{
				source: '/donate',
				destination: 'https://www.paypal.com/donate/?business=VN2W6K8NSXFS6&no_recurring=0&currency_code=EUR',
				permanent: false,
			},
		];
	},
	webpack(config) {
		config.module.rules.push({
			test: /\.svg$/,
			use: ['@svgr/webpack'],
		});

		return config;
	},
};

module.exports = nextConfig;
