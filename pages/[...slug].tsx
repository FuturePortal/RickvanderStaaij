import React, { ReactElement } from 'react';

import { NextPageContext } from 'next';

import NotFound from 'components/Layout/NotFound';

const Page = (): ReactElement => <NotFound />;

Page.getInitialProps = async ({ res: response }: NextPageContext) => {
	if (typeof window === 'undefined') {
		response.statusCode = 404;
	}
	return {};
};

export default Page;
