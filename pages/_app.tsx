import React, { ReactNode } from 'react';

import App from 'next/app';
import { AppProps } from 'next/app';
import { AppContext } from 'next/dist/pages/_app';

import GoogleAnalytics from 'components/GoogleAnalytics';
import Layout from 'components/Layout';

import 'minireset.css/minireset.css'; // @TODO
import 'animate.css'; // @TODO
import 'style/global.scss';

const MyApp = ({ Component, pageProps }: AppProps): ReactNode => (
	<Layout>
		{process.env.APP_ENV === 'production' && <GoogleAnalytics />}
		<Component {...pageProps} />
	</Layout>
);

/**
 * Note: When this function is ran server side, it will send the result as JSON.stringy() to the client side
 * which will initialize the app there with that data. Every new client side page load will run this function
 * again on the client.
 */
MyApp.getInitialProps = async (appContext: AppContext) => {
	if (typeof window === 'undefined') {
		// do nothing
	}

	const appProps = await App.getInitialProps(appContext);

	return {
		...appProps,
	};
};

export default MyApp;
