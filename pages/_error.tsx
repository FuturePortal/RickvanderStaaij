import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import PageMeta from 'components/Layout/PageMeta';
import useStyle from 'components/Layout/useStyle';

const Page = (): ReactElement => {
	const { className: animation, style: animationStyle } = useAnimation('fadeInUp', 'fadeOutDown');
	const style = useStyle();

	return (
		<div {...style('section')}>
			<div {...style('container', animation)} style={animationStyle}>
				<PageMeta title="Borky borky" description="Whoops, something went wrong here :(" />
				<h1>What the fuck?! Shit broke</h1>
				<p>Whoops, something went wrong. Please use the navigation to get out of this mess.</p>
			</div>
		</div>
	);
};

Page.getInitialProps = ({ res, err }) => {
	const statusCode = res ? res.statusCode : err ? err.statusCode : 404;

	return { statusCode };
};

export default Page;
