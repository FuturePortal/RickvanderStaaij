import React from 'react';

import { ImageResponse } from '@vercel/og';
import { NextRequest } from 'next/server';

export const config = {
	runtime: 'experimental-edge',
};

const font = fetch(new URL('../../public/fonts/Nunito-Regular.ttf', import.meta.url)).then((res) => res.arrayBuffer());

export default async function handler(req: NextRequest) {
	const { searchParams } = req.nextUrl;
	const title = searchParams.get('title') ?? 'rick.nu';
	const fontData = await font;

	return new ImageResponse(
		(
			<div
				style={{
					display: 'flex',
					fontSize: 60,
					color: '#fff',
					background: '#0e7795',
					width: '100%',
					height: '100%',
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'center',
					textAlign: 'center',
					fontFamily: 'Nunito',
				}}
			>
				<img
					width="320"
					height="320"
					src={`https://avatars.githubusercontent.com/u/6495166?v=4`}
					style={{ borderRadius: 160 }}
				/>
				<p style={{ padding: 0, paddingLeft: 100, paddingRight: 100, margin: 0 }}>{title}</p>
				<p style={{ padding: 0, margin: 0, marginTop: 20, fontSize: 40, color: '#1a1a1a' }}>
					Rick van der Staaij • https://rick.nu
				</p>
			</div>
		),
		{
			width: 1200,
			height: 630,
			fonts: [
				{
					name: 'Nunito',
					data: fontData,
					style: 'normal',
				},
			],
		}
	);
}
