import type { NextApiRequest, NextApiResponse } from 'next';
import NodeMailer from 'nodemailer';
import { Options as NodeMailerOptions } from 'nodemailer/lib/smtp-transport';

import { ContactFormValues } from 'components/Sections/Contact/ContactForm/ContactForm';

const buildMailContent = (unsafe: string, withNewlines = false): string | undefined => {
	let safe = unsafe
		.replaceAll('&', '&amp;')
		.replaceAll('<', '&lt;')
		.replaceAll('>', '&gt;')
		.replaceAll('"', '&quot;')
		.replaceAll("'", '&#039;');

	if (withNewlines) {
		safe = safe.replaceAll('\n', '<br />');
	}

	return safe;
};

export default async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
	console.log('Sending mail endpoint.');

	const data = req.body as Partial<ContactFormValues>;

	const requiredFields = ['name', 'subject', 'contact', 'message'];

	for (const field of requiredFields) {
		if (!req.body[field]) {
			console.log('Failed to send mail:');
			console.error(`Missing required field: ${field}.`);
			res.statusCode = 422;
			res.json({ message: `Missing required field: ${field}.` });
			return;
		}
	}

	const transporter = NodeMailer.createTransport({
		host: process.env.MAIL_SMTP!,
		port: process.env.MAIL_PORT!,
		auth: {
			user: process.env.MAIL_USER!,
			pass: process.env.MAIL_PASSWORD!,
		},
	} as unknown as NodeMailerOptions);

	console.log('Created transport.');

	const subject = buildMailContent(data.subject);
	const contact = buildMailContent(data.contact);
	const name = buildMailContent(data.name);
	const message = buildMailContent(data.message, true);

	try {
		const info = await transporter.sendMail({
			from: process.env.MAIL_TARGET,
			to: process.env.MAIL_TARGET,
			subject: `Contact: ${subject}`,
			text: `${subject} - ${name} - ${contact} - ${message}`,
			html: `
			<html lang="en">
				<p><strong>Subject:</strong><br />${subject}</p>
				<p><strong>Name:</strong><br />${name}</p>
				<p><strong>Contact:</strong><br />${contact}</p>
				<p><strong>Message:</strong><br />${message}</p>
			</html>`,
		});

		console.log(`Mail sent: ${info.messageId}`);

		res.statusCode = 200;
		res.json({ message: 'Your mail is now sent, thank you!' });
	} catch (error) {
		console.log('Failed to send mail:');
		console.error(error);
		res.statusCode = 500;
		res.json({ message: 'Failed to send a mail, please try again later.' });
	}
};
