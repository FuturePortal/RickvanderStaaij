import React, { ReactElement } from 'react';

import PageMeta from 'components/Layout/PageMeta';
import Timeline from 'components/Timeline';

const Page = (): ReactElement => (
	<>
		<PageMeta title="My career on a timeline" description="View the career of Rick van der Staaij on a timeline." />
		<Timeline />
	</>
);

export default Page;
