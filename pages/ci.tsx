import React, { ReactElement } from 'react';

import CIMonitor from 'components/CIMonitor';
import PageMeta from 'components/Layout/PageMeta';

const Page = (): ReactElement => (
	<>
		<PageMeta
			title="CI/CD status monitor"
			description="A live overview of my latest continuous integration/deployment statuses."
		/>
		<CIMonitor />
	</>
);

export default Page;
