import React, { ReactElement } from 'react';

import PageMeta from 'components/Layout/PageMeta';
import ContactSection from 'components/Sections/Contact';

const Page = (): ReactElement => (
	<>
		<PageMeta title="Contact me" description="Check all the different ways to contact Rick." />
		<ContactSection />
	</>
);

export default Page;
