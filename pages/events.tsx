import React, { ReactElement } from 'react';

import moment from 'moment';

import useAnimation from 'components/Animation/useAnimation';
import Countdown from 'components/Countdown';
import PageMeta from 'components/Layout/PageMeta';

const Page = (): ReactElement => {
	const animation = useAnimation('fadeInDown', 'fadeOutDown');

	return (
		<>
			<PageMeta title="Events" description="Listing important events I will be participating in." />
			<div {...animation}>
				<div className="section">
					<div className="container">
						<h1>Events</h1>
						<p>Events I&apos;m participating in, or things I&apos;m looking forward to! Meet me there?</p>
					</div>
				</div>
				<div className="section" style={{ background: '#F0F0F0' }}>
					<div className="container">
						<h2 style={{ textAlign: 'center' }}>Strong Viking | Obstacle Run Wijchen | Beast 19KM</h2>
						<Countdown target={moment('2025-04-12 09:00')} />
					</div>
				</div>
				<div className="section">
					<div className="container">
						<h2 style={{ textAlign: 'center' }}>Strong Viking | Obstacle Run Wijchen | Beast 19KM</h2>
						<Countdown target={moment('2025-06-14 09:00')} />
					</div>
				</div>
				<div className="section" style={{ background: '#F0F0F0' }}>
					<div className="container">
						<h2 style={{ textAlign: 'center' }}>Strong Viking | Obstacle Run Amsterdam | Beast 19KM</h2>
						<Countdown target={moment('2025-09-27 09:00')} />
					</div>
				</div>
				<div className="section">
					<div className="container">
						<h2 style={{ textAlign: 'center' }}>34 years old</h2>
						<Countdown target={moment('2025-11-08')} />
					</div>
				</div>
			</div>
		</>
	);
};

export default Page;
