import React, { ReactElement } from 'react';

import { NextPageContext } from 'next';
import { useRouter } from 'next/router';

import highlights from 'components/Highlights';
import Highlight from 'components/Highlights/Highlight';
import NotFound from 'components/Layout/NotFound';
import PageMeta from 'components/Layout/PageMeta';

const getHighlight = (slug) => highlights.find((creation) => creation.slug === slug);

const Page = (): ReactElement => {
	const router = useRouter();
	const highlight = getHighlight(router.query.slug);

	if (!highlight) {
		return <NotFound />;
	}

	return (
		<>
			<PageMeta title={highlight.title} description={highlight.intro} />
			<Highlight highlight={highlight} />
		</>
	);
};

Page.getInitialProps = async ({ res: response, query }: NextPageContext) => {
	const creation = getHighlight(query.slug);

	if (!creation) {
		if (typeof window === 'undefined') {
			response.statusCode = 404;
		}
	}

	return {};
};

export default Page;
