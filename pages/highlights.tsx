import React, { ReactElement } from 'react';

import useAnimation from 'components/Animation';
import highlights from 'components/Highlights';
import HighlightList from 'components/Highlights/List';
import PageMeta from 'components/Layout/PageMeta';

const Page = (): ReactElement => {
	const animation = useAnimation();

	return (
		<div className="section">
			<div className="container">
				<PageMeta title="My highlights" description="An overview of Rick's highlights." />
				<div {...animation}>
					<h1>Highlights</h1>
					<p>Things I&apos;ve created, or what I&apos;m proud of.</p>
				</div>
				<HighlightList highlights={highlights} />
			</div>
		</div>
	);
};

export default Page;
