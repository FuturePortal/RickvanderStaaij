import React, { ReactElement } from 'react';

import PageMeta from 'components/Layout/PageMeta';
import AboutMe from 'components/Sections/AboutMe';
import HighlightTeaser from 'components/Sections/HighlightTeaser';
import HobbyImages from 'components/Sections/Hobbies';
import TimelineTeaser from 'components/Sections/TimelineTeaser';

const Page = (): ReactElement => {
	return (
		<>
			<PageMeta title="About me" description="The personal website of Rick van der Staaij." />
			<AboutMe />
			<HighlightTeaser />
			<TimelineTeaser />
			<HobbyImages />
		</>
	);
};

export default Page;
