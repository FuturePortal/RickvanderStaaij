import React, { ReactElement } from 'react';

import PageMeta from 'components/Layout/PageMeta';
import Recommendations from 'components/Recommendations';

const Page = (): ReactElement => {
	return (
		<>
			<PageMeta title="Recommendations" description="People who think I could be an addition to your team." />
			<Recommendations />
		</>
	);
};

export default Page;
