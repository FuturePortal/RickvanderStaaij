import { NextPage, NextPageContext } from 'next';

const getProductionRobots = (domain: string): string => `User-agent: *
Allow: /
Disallow: /_next/

sitemap: ${domain}/sitemap.xml
`;

const getDevelopmentRobots = (domain: string): string => `User-agent: *
Disallow: /

sitemap: ${domain}/sitemap.xml
`;

const getRobots = (domain: string): string => {
	if (process.env.APP_ENV === 'production') {
		return getProductionRobots(domain);
	}

	return getDevelopmentRobots(domain);
};

const RobotsTxt = (): NextPage => null;

RobotsTxt.getInitialProps = async ({ res: response }: NextPageContext): Promise<void> => {
	const domain = process.env.NEXT_PUBLIC_APP_URL;

	response.setHeader('Content-Type', 'text/plain');
	response.write(getRobots(domain));
	response.end();
};

export default RobotsTxt;
