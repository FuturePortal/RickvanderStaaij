import React, { ReactElement } from 'react';

import PageMeta from 'components/Layout/PageMeta';
import Specifications from 'components/Sections/Specifictions';

const Page = (): ReactElement => (
	<>
		<PageMeta title="Site specifications" description="What makes this website awesome for developers?" />
		<Specifications />
	</>
);

export default Page;
