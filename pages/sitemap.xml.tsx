import { NextPage, NextPageContext } from 'next';

import highlights from 'components/Highlights';

const buildSiteMapXML = (links: string[]): string => {
	return `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">${links.join('')}
</urlset>`;
};

const addLink = (links: string[], domain: string, url: string, priority = 1): void => {
	links.push(`
    <url>
        <loc>${domain}${url}</loc>
        <priority>${priority}</priority>
        <changefreq>weekly</changefreq>
    </url>`);
};

const RobotsTxt = (): NextPage => null;

const pages = ['/', '/career', '/highlights', '/site-specifications', '/ci', '/contact', '/recommendations'];

RobotsTxt.getInitialProps = async ({ res: response }: NextPageContext): Promise<void> => {
	const links = [];

	const domain = process.env.NEXT_PUBLIC_APP_URL;

	// Add home page
	pages.map((page) => addLink(links, domain, page));

	// Add highlights
	highlights.map((page) => addLink(links, domain, `/highlight/${page.slug}`));

	// Return sitemap.xml
	response.setHeader('Content-Type', 'application/xml');
	response.write(buildSiteMapXML(links));
	response.end();
};

export default RobotsTxt;
