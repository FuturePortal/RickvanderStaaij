/** @type {import('stylelint').Config} */
export default {
	extends: ['stylelint-config-recess-order', 'stylelint-config-recommended-scss'],
	rules: {
		'color-hex-length': 'long',
		'order/properties-alphabetical-order': null,
		'selector-no-qualifying-type': null,
		'max-nesting-depth': 5,
		'selector-class-pattern': null,
		'scss/comment-no-empty': null,
		'selector-pseudo-class-no-unknown': [
			true,
			{
				ignorePseudoClasses: ['global'],
			},
		],
	},
};
