import { Moment } from 'moment';

declare global {
	type CIState = 'info' | 'warning' | 'error' | 'success';

	type CIStepState = 'created' | 'pending' | 'running' | 'success' | 'failed' | 'soft-failed' | 'skipped' | 'timeout';

	type CIStep = {
		id: string;
		title: string;
		state: CIStepState;
		time: string;
		duration?: number;
	};

	type CIStage = {
		id: string;
		title?: string;
		state: CIStepState;
		steps: CIStep[];
		time: string;
	};

	type CIProcess = {
		id: number;
		title: string;
		state: CIState;
		stages: CIStage[];
		time: string;
		duration?: number;
	};

	type CIStatus = {
		id: string;
		project: string;
		state: CIState;
		processes: CIProcess[];
		time: string;
		source: 'github' | 'gitlab' | 'readthedocs';
		source_url?: string;
		url?: string;
		branch?: string;
		tag?: string;
		issue?: number;
		projectImage?: string;
		userImage?: string;
		mergeTitle?: string;
		mergeUrl?: string;
	};

	type CITest = {
		time: Moment;
	};
}
