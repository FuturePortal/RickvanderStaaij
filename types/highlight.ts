import { Moment } from 'moment';
import { StaticImageData } from 'next/image';
import { ReactElement } from 'react';

export type Highlight = {
	title: string;
	slug: string;
	date: Moment;
	intro: string;
	content: ReactElement;
	cover: StaticImageData;
};
