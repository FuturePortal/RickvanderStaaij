import { Moment } from 'moment';

declare global {
	type TimelineEvent = {
		title: string;
		date: Moment;
		till?: Moment;
		type: 'video' | 'job' | 'internship' | 'diploma' | 'blog' | 'celebration';
		content?: ReactElement;
	};
}
